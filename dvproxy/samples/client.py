"""

    a sample http client usage


"""

from dvproxy.httpclient import HttpClient


url= "http://localhost:5000"

users= ['Alice','Bob']

#         identifier type         parameters
cnf= [
        [ 'Alice',   'sypterm',   {}           ],
        [ 'Bob'  ,   'droydterm', {}         ]
    ]


class MyClient(HttpClient):
    """

    """




def test_basic():


    #client= HttpClient(url=url,agent_factory=agent_factory)
    client= MyClient(url=url)

    #configuration= []
    #for order,user in enumerate(users):
    #    configuration.append([user,cnf[order]])

    configuration= cnf

    client.open_session(configuration,session_type=None)

    res= client.dummy('Alice', p1="v1",p2="v2")
    res= client.dummy('Bob', p3="v3")

    try:
        # call with a bad function
        res= client.bad_function('Bob', p3="v3")
    except RuntimeError:
        pass

    try:
        # call with a user
        res= client.bad_function('Unknown', p3="v3")
    except RuntimeError:
        pass



    #res= client.agent('Alice').dummy( p1="v1",p2="v2")
    #res= client.agent('Bob').dummy( p3="v3")

    client.close_session()

    return



test_basic()
