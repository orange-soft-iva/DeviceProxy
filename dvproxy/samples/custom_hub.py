
from dvproxy.hub import Hub
from dvproxy.agent import Agent


class CustomAgent(Agent):
    """

    """
    version= 'base'
    def dummy(self,**kwargs):
        """
            a dummy operation returning the given arguments

        :param arg:
        :param kwargs:
        :return:
        """
        print "Agent %s is a (%s): dummy(%s)" % (self.identifier,self.version,str(kwargs))
        return kwargs

class SypTerm(CustomAgent):
    """
    """
    version= 'sypterm'
    def setup(self,create=False):
        """

        :param identifier:
        :param parameters:
        :return:
        """
        print "Sypterm.__init__(identifier=%s,parameters=%s)" % (str(self.identifier),str(self.parameters))

class DroydTerm(CustomAgent):
    """
    """
    version= 'droydterm'

    def setup(self,create=False):
        """

        :param identifier:
        :param parameters:
        :return:
        """
        print "Droydterm.__init__(identifier=%s,parameters=%s)" % (str(self.identifier),str(self.parameters))


class CustomHub(Hub):
    """


    """
    allow_session_reopen = True





if __name__=="__main__":



    sample_conf= [
        [ 'Alice', 'sypterm', {} ],
        [ 'Bob'  , 'droydterm', {} ]
    ]


    hub= CustomHub()

    s= hub.open_session(sample_conf)


    hub.dummy('Alice',p1='p1', p2='p2')
    hub.dummy('Bob', p1='pp')

    #hub.agent('Alice').dummy(p1='p1', p2='p2')
    #hub.agent('Bob').dummy( p1='pp')



    hub.close_session()


    print