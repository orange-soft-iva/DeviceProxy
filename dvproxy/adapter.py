
__author__ = 'cocoon'
"""


    adapter adapt a process to receive command lines from a redis queue and relay output to another redis queue


    # create an adapter
    a = ThreadedAdapter($agent_id, "python term.py")
    a.start()

    # use adapter
    d= ClientAdapter($agent_id)
    d.send('cmd\n')
    response= d.read()
    d.exit()

    note: you need a redis-server

    read commands from redis queue /agents/$agent_id/stdin

        and write it to term stdin


    read output from term stdout

        and write it to redis queue /agents/$agent_id/stout



"""

import sys
import fcntl
import os
import time
import subprocess
import redis

import threading
import multiprocessing

class RedisQueue(object):
    """Simple Queue with Redis Backend"""
    def __init__(self, name, namespace='queue', **redis_kwargs):
        """The default connection parameters are: host='localhost', port=6379, db=0"""
        self.__db= redis.Redis(**redis_kwargs)
        self.key = '%s:%s' %(namespace, name)

    def qsize(self):
        """Return the approximate size of the queue."""
        return self.__db.llen(self.key)

    def empty(self):
        """Return True if the queue is empty, False otherwise."""
        return self.qsize() == 0

    def put(self, item):
        """Put item into the queue."""
        self.__db.rpush(self.key, item)

    def get(self, block=True, timeout=None):
        """Remove and return an item from the queue.

        If optional args block is true and timeout is None (the default), block
        if necessary until an item is available."""
        if block:
            item = self.__db.blpop(self.key, timeout=timeout)
        else:
            item = self.__db.lpop(self.key)

        if item:
            item = item[1]
        return item

    def get_nowait(self):
        """Equivalent to get(False)."""
        return self.get(False)


# def non_block_read(output):
#     fd = output.fileno()
#     fl = fcntl.fcntl(fd, fcntl.F_GETFL)
#     fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)
#     try:
#         return output.read()
#     except:
#         return ""


class AdapterBase(object):
    """
        Base class for Adapter, ClientAdapter

    """
    key_prefix= "/agents"
    cmd_exit= 'exit\n'

    redis_kwargs={}
    agent_id= None
    trace_enabled= True
    db=None

    Terminated=False


    def trace(self, s):
        if self.trace_enabled:
            #now = time.time()
            #fmt = "================== " + s + " ==================" + " [at t=%(time)03d]"
            #self.log(fmt % {'time': int(now - self.t0)})
            print s


    def queue_name(self,name):
        """

        """
        return '%s/%s/%s' % (self.key_prefix, self.agent_id,name)


    def status(self):
        """
            return the contents of hash /agents[agent_id]
        """
        stat= self.db.hget(self.key_prefix,self.agent_id)
        return stat

    def agents_status(self):
        """
            list status of  all declared agents

        :return:
        """
        return self.db.hgetall(self.key_prefix)

    def stop(self):
        """

        :return:
        """
        self.Terminated= True



class Adapter(AdapterBase):
    """
        adapt a process to be driven via redis queues

    """

    def __init__(self,agent_id,command_line,shell=True,key_prefix='/agents',**redis_kwargs):
        """

        :param agent_id:
        :param command_line:
        :return:
        """
        self.agent_id= agent_id
        self.key_prefix= key_prefix
        self.redis_kwargs= redis_kwargs

        self.db= redis.Redis(** self.redis_kwargs)

        self.q_in = RedisQueue(self.queue_name('stdin'),**self.redis_kwargs)
        self.q_out= RedisQueue(self.queue_name('stdout'),**self.redis_kwargs)

        self.command_line= command_line
        self.shell= shell

        self.proc= None
        self.Terminated= False


    def run_process(self,fullcmd=None,shell=None):
        """
            launch the slave process
        """
        fullcmd= fullcmd or self.command_line
        shell = shell or self.shell
        self.trace("Popen " + fullcmd  )
        self.proc = subprocess.Popen(fullcmd, shell=shell, bufsize=0, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                                     universal_newlines=False)
        self.started = self.proc.poll()
        return self.started

    def is_running(self):
        """
            return the subprocess status

        """
        if self.proc is None:
            #  proc has not been started
            return False
        status = self.proc.poll()
        if status == None:
            # process is still running
            return True
        else:
            # process has returned
            self._status = status
            return False

    def send(self, cmd):
        """
            send command to process
        :param cmd:
        :return:
        """
        #self.trace("send " + cmd)
        if not cmd.endswith('\n'):
            cmd += '\n'
        self.proc.stdin.writelines(cmd)
        self.proc.stdin.flush()

    def read(self,wait=False):
        """
            read output from process

        """
        if wait:
            line = self.proc.stdout.readline()
        else:
            output= self.proc.stdout
            fd= output.fileno()
            fl = fcntl.fcntl(fd, fcntl.F_GETFL)
            fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)
            try:
                return output.read()
            except:
                return ""

        return line


    def shutdown_process(self):
        """
            kill slave process
        """
        if sys.hexversion >= 0x02060000:
            self.proc.terminate()
        else:
            self.proc.wait()
        return


    def transfer_in(self):
        """

            transfer redis stdin input to slave stdin ( if any )

        :return:
        """
        # read redis queue in
        incoming_cmd= self.q_in.get(timeout= 1)
        if incoming_cmd:
            if incoming_cmd == self.cmd_exit:
                # exit condition, stop slave process and exit main loop
                #self.shutdown()
                self.Terminated=True
            else:
                self.send(incoming_cmd)


    def transfer_out(self):
        """

            transfer slave stdout input to redis stdout ( if any )

        :return:
        """
        # read redis_queue_out
        slave_out= self.read()
        if slave_out:
            self.q_out.put(slave_out)

    def reset_queue(self,queue):
        """

        :param queue: redis queue, like self.q_in or self.q_out
        :return:
        """
        while True:
            dummy= queue.get_nowait()
            if not dummy:
                break


    def keep_alive(self,clear=False):
        """

            write time to /agents[agent_id]
        :return:
        """
        if clear:
            # clear keep alive for this agent
            self.db.hset( self.key_prefix,self.agent_id,None)
        else:
            # set keep alive to current time for this agent
            self.db.hset( self.key_prefix,self.agent_id,time.time())

    def get_keep_alive(self):
        """

        :return:
        """
        return self.db.hget(self.key_prefix,self.agent_id)


    def run(self):
        """
            main loop

            read from /agents/?/stdin
            if not empty :
                send to slave stdin

            read from slave output
                if not empty :
                    push to /agents/?/stdout

        :return:
        """
        self.trace("starting process for agent: %s" % self.agent_id)

        # reset queues
        self.keep_alive()
        self.reset_queue(self.q_in)
        self.reset_queue(self.q_out)

        # launch slave process
        self.run_process()
        self.Terminated= False

        while not self.Terminated:
            # infinite loop  till 'exit' received via command line
            self.keep_alive()
            self.transfer_out()
            self.transfer_in()
            # exit if 'exit' received through /agent/?/stdin

        # kill slave process and reset keep_alive
        self.shutdown_process()
        self.keep_alive(clear=True)
        sys.exit(0)


class ThreadedAdapter(Adapter,threading.Thread):
    """
        a threading version of adapter

    """
    def __init__(self,agent_id,command_line,**kwargs):
        """

        """
        threading.Thread.__init__(self)
        Adapter.__init__(self,agent_id,command_line,**kwargs)
        self.Terminated=False
        self.setDaemon(True)

    # def run(self):
    #     self.trace('starting thread ')
    #     return Adapter.run(self)
    #
    # def stop(self):
    #     self.Terminated= True

class ProcessAdapter(Adapter,multiprocessing.Process):
    """
        a threading version of adapter

    """
    def __init__(self,agent_id,command_line,**kwargs):
        """

        """
        multiprocessing.Process.__init__(self)
        Adapter.__init__(self,agent_id,command_line,**kwargs)
        self.Terminated=False
        #self.setDaemon(True)



class ClientAdapter(AdapterBase):
    """
        a client to send data to adapters

    """

    def __init__(self,agent_id,key_prefix='/agents',**redis_kwargs):
        """

        """
        self.agent_id=agent_id
        self.redis_kwargs= redis_kwargs
        self.db= redis.Redis(** self.redis_kwargs)
        self.key_prefix= key_prefix

        self.q_in = RedisQueue(self.queue_name('stdin'),**self.redis_kwargs)
        self.q_out= RedisQueue(self.queue_name('stdout'),**self.redis_kwargs)

    def send(self,cmd):
        """
            send command to redis queue
        """
        return self.q_in.put(cmd)

    def read(self,timeout=1):
        """
        """
        if timeout:
            return self.q_out.get(timeout=timeout)
        else:
            return self.q_out.get_nowait()


    def keep_alive(self):
        """
            return the timestamp of agent keep alive
        """
        return self.db.hget(self.key_prefix,self.agent_id)

    ###
    def exit(self):
        """
            send exit to agent adapter
        """
        self.send(self.cmd_exit)


    def wait(self,counter=15):
        """
            wait until adapter is done (eg keep alive is None)
        """
        t1= 0
        t2=0
        while counter > 0:
            t2=t1
            t1 = self.keep_alive()
            if t1 is None:
                # OK adapter is done
                return True
            time.sleep(1)
            counter = counter -1
        # still alive
        if t1==t2:
            print("adapter is freezed, last timestamp: %s" % t1)
        else:
            print("adapter is still alive, last timestamp: %s" % t1)

        return False


#
#  adapter command line interface
#

"""
    command line


    adapter agent_id  --cmd "python term.py"



    adapter run agent_id --cmd "python term.py"

    adapter status [--agent agent_id] [--kill]


    adapter exit <agent_id>
    adapter send <agent_id> <msg> --wait 2






"""



import click

@click.group()
def cli():
    pass


# # run command
# @click.command()
# @click.argument('agent_id')
# @click.option('--cmd',required=True)
# @click.option('--key-prefix',default="/agents")
# def run(agent_id,cmd,key_prefix,**kwargs):
#     click.echo('launch adapter for %s with command: %s' % (agent_id,cmd))
#     click.echo('* to send command to process, write to redis queue in: /agents/%s/stdin' % agent_id)
#     click.echo('* to read process response, read from redis queue out: /agents/%s/stdout'% agent_id)
#     click.echo('* to stop process, send "exit\\n" to redis queue in')
#     adapter= ProcessAdapter(agent_id,command_line=cmd)
#     adapter.start()
#     pass
#
# cli.add_command(run)


# redis command
@click.group('redis')
def redis_group():
    pass

@redis_group.command('agents')
@click.option('--status',is_flag=True)
@click.option('--kill',is_flag=True)
@click.option('--agent')
def agents(**kwargs):
    """

    :param agent_id:
    :param queue:
    :return:
    """
    if kwargs['agent']:
        # operation on a single agent
        agent_name= kwargs['agent']
        a= ClientAdapter(agent_name)
        #agents= a.status()
        #status= agents[agent_name]

        if kwargs['status']:
            status= a.status()
            click.echo("agent [%s] , status is %s" %(agent_name,status))

        if kwargs['kill']:
            a.exit()
            click.echo("kill agent %s" % agent_name)
    else:
        # operation on all agents
        all=ClientAdapter('dummy')
        if kwargs['status']:
            click.echo('agent list:')
            agents= all.agents_status()
            for agent,status in agents.iteritems():
                click.echo("agent [%s] , status is %s" %(agent,status))
        if kwargs['kill']:
            agents= all.agents_status()
            for agent,status in agents.iteritems():
                if status is not None:
                    a= ClientAdapter(agent)
                    a.exit()
                    click.echo("kill agent %s" % agent)



cli.add_command(agents)


# interact with an agent
#
# run agent_id cmd
# send agent_id msg
# read agent_id --wait
# kill agent_id
# status agent_id

@click.group('agent')
#@click.argument('agent_id')
def agent():
    pass


# run command
@agent.command('run')
@click.argument('agent_id')
@click.option('--cmd',required=True)
def agent_run(agent_id,cmd,**kwargs):
    """
        run an adapter to a process
    """
    click.echo('launch adapter for %s with command: %s' % (agent_id,cmd))
    click.echo('* to send message to process: adapter send  %s "message"' % agent_id)
    click.echo('* to read process response, adapter read %s --timeout 1'% agent_id)
    click.echo('* to stop process, adapter kill %s' %  agent_id)
    adapter= ProcessAdapter(agent_id,command_line=cmd)
    adapter.start()
    return 0

@agent.command('send')
@click.argument('agent_id')
@click.argument("message")
def agent_send(agent_id,message,**kwargs):
    """

    :param agent_id:
    :param message:
    :param kwargs:
    :return:
    """
    click.echo("send command to agent %s : %s" % (agent_id,message))
    a= ClientAdapter(agent_id)
    r= a.send(message)
    return 0


@agent.command('read')
@click.argument('agent_id')
@click.option('--timeout')
def agent_read(agent_id,timeout=0,**kwargs):
    """

    :param agent_id:
    :param timeout:
    :param kwargs:
    :return:
    """
    click.echo("read message from agent %s" % agent_id)
    a= ClientAdapter(agent_id)
    if timeout==0:
        response= a.read(timeout=0)
    else:
        response= a.read(timeout)
    click.echo("message is:\n %s" % response)
    return 0

@agent.command('kill')
@click.argument('agent_id')
def agent_kill(agent_id,timeout=0,**kwargs):
    """

    :param agent_id:
    :param timeout:
    :param kwargs:
    :return:
    """
    click.echo("kill agent %s" % agent_id)
    a= ClientAdapter(agent_id)
    a.exit()
    return 0

@agent.command('status')
@click.argument('agent_id')
def agent_status(agent_id,**kwargs):
    """

    :param agent_id:
    :param timeout:
    :param kwargs:
    :return:
    """
    a= ClientAdapter(agent_id)
    status= a.status()
    click.echo("status of [%s] is %s" % (agent_id,status))
    return 0

cli.add_command(agent)



if __name__=="__main__":

    cli()

    pass