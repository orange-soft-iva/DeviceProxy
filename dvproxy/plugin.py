__author__ = 'cocoon'
"""

    a base type (metaclass)  for plugin system

    code from : http://martyalchin.com/2008/jan/10/simple-plugin-framework/


    about python and plugin systems: http://wehart.blogspot.fr/2009/01/python-plugin-frameworks.html






"""

class PluginMount(type):
    """
        The base of the plugin system
    """
    def __init__(cls, name, bases, attrs):
        if not hasattr(cls, 'plugins'):
            # This branch only executes when processing the mount point itself.
            # So, since this is a new plugin type, not an implementation, this
            # class shouldn't be registered as a plugin. Instead, it sets up a
            # list where plugins can be registered later.
            cls.plugins = []
        else:
            # This must be a plugin implementation, which should be registered.
            # Simply appending it to the list is all that's needed to keep
            # track of it later.
            cls.plugins.append(cls)

    def get_plugins(cls, *args, **kwargs):
        return [p(*args, **kwargs) for p in cls.plugins]

    def select_plugin(cls,role,version=None):
        """
            role is either user, workspace, run , report

        """
        roles= []
        for item in cls.plugins:
            if item.role == role:
                roles.append(item)
                if version and version == item.version:
                        return item

        # not found , return the first of the list
        return roles[0]






if __name__=="__main__" :


    class WorkspaceProvider:
        """
        Mount point for plugins which refer to actions that can be performed.

        Plugins implementing this reference should provide the following attributes:

        ========  ========================================================
        title     The text to be displayed, describing the action

        url       The URL to the view where the action will be carried out

        selected  Boolean indicating whether the action is the one
                  currently being performed
        ========  ========================================================
        """
        __metaclass__ = PluginMount

        model= None


        def __init__(self,parent,name, create=False,**parameters):
            """

            :param parent:
            :param name:
            :param create:
            :param parameters:
            :return:
            """
            self.parent=parent
            self.name=name
            self.parameters= parameters


    class WorkspaceManager(WorkspaceProvider):
        """

        """
        model= 'base'

    class QcWorkspaceManager(WorkspaceProvider):
        """

        """
        model= 'qc'


    #l= ActionProvider.get_plugins('hello')

    workspace_providers= WorkspaceProvider.plugins

    qc_workspace_class= WorkspaceProvider.select_plugin('qc')


