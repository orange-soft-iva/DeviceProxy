from plugin import PluginMount
"""

    a base class for an Agent


"""
import logging
log= logging.getLogger(__name__)


class Agent(object):
    """

        a base Api for an agent

        __init__( identifier, create=False ,** parameters )
        setup(create=False)
        close()
        dummy(**kwargs)

    """
    __metaclass__ = PluginMount

    role= 'agent'
    version = None


    def __init__(self, identifier, create= False,**parameters):
        """

        :param args:
        :param kwarg:
        :return:
        """
        self.identifier= identifier
        self.parameters= parameters
        self.setup(create)
        return

    def setup(self,create=False):
        """

        :param identifier:
        :param parameters:
        :return:
        """
        print "Agent.__init__(identifier=%s,parameters=%s)" % (str(self.identifier),str(self.parameters))

    def dummy(self,**kwargs):
        """
            a dummy operation returning the given arguments

        :param arg:
        :param kwargs:
        :return:
        """
        print "Agent %s: dummy(%s)" % (self.identifier,str(kwargs))
        return kwargs

    def close(self,**kwargs):
        """
        """
        return True

    @classmethod
    def guess_agent_id(cls,identifier,**parameters):
        """
            return the identifier of the agent ( for lock)
            by default agent_id is self.identifier

            other possibilities : return parameters['agent_id']
        """
        return identifier

if __name__=="__main__" :



    class AgentOne(Agent):
        """

        """
        version= 'one'

        def dummy(self,**kwargs):
            print "Agent %s of type [one]: dummy(%s)" % (self.identifier,str(kwargs))


    class AgentTwo(Agent):
        """

        """
        version= 'two'


        def dummy(self,**kwargs):
            print "Agent %s of type [Two]: dummy(%s)" % (self.identifier,str(kwargs))


    #

    agent_factory_1= Agent.select_plugin('agent','one')
    agent_factory_2= Agent.select_plugin('agent','two')


    a1= agent_factory_1('id1')
    a2= agent_factory_2('id2')

    a1.dummy()
    a2.dummy()


    print 'Done'


