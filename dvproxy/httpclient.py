__author__ = 'cocoon'
"""

    HTTP api to hub server


        open_session
        close_session

        + all agent operations ( click ,press_home ...)




"""

import sys
import json
import requests


# default log_cb
stdout_no_buf = True

def out(msg,nl=True):
    """
        send message to stdout and flush
    """
    if nl:
        # newline : print msg + nl
        print msg
    else:
        # send without new line
        print msg,

    if stdout_no_buf:
        sys.stdout.flush()





class ApplicationSession():
    """

    """
    def __init__(self):
        """
        """
        self._store = {
            'accounts' : {},
            'current':  {}
        }



    def add_users(self,*accounts):
        """

        """
        for account in accounts:
            #key,param= account
            key= account[0]
            param= account[1:]
            self._store['accounts'][key]= param



    def current_session(self,data=None):
        """
            current session info
        """
        if not data:
            # read it
            return self._store['current']
        else:
            # set data
            self._store['current'] = data

    def close(self):
        """
        """
        self._store = {
            'accounts' : {},
            'current':  {}
        }


class HttpClient(object):
    """


        HttpClient(url )


    """

    def __init__(self,url,web_session=None):
        """

        :param url:
        :return:
        """
        # the base url eg localhost:5000
        self._url = url
        self._web_session= None

        if web_session is not None:
            self._web_session= web_session
        else:
            self._web_session = self.set_web_session()


        # # set the requests session
        # self._web_session=requests.Session()
        # self._web_session.headers.update({
        #     'content-type': 'application/json; charset=utf-8',
        #     'accept': 'application/json',
        #     'Accept-Charset': 'utf-8'
        #     })


        # the phone session
        self._session = None


    def set_web_session(self):
        """
        # set the requests session

        :param url:
        :return:
        """
        if self._web_session is None:

            self._web_session=requests.Session()
            self._web_session.headers.update({
                'content-type': 'application/json; charset=utf-8',
                'accept': 'application/json',
                'Accept-Charset': 'utf-8'
                })

        return self._web_session


    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """

        :param exc_type:
        :param exc_val:
        :param exc_tb:
        :return:
        """
        #self._store.close()
        if self._session:
            self._session.close()

    def url(self, uri = None):
        """

        :param complement:
        :return:
        """
        if not uri:
            return self._url
        else:
            #return "/".join(self._url , uri )
            return "%s%s" % (self._url,uri)



    def open_session(self,configuration,**parameters):
        """

            open a session with configuration
                store accounts

         :param configuration:list, configuration of the session list of dict name,type,paramameters
         :param parameters: dict : parameters for the session type

         example of parameters: session_type, session_id ...
        :return:
        """
        #agent_ids= [ element[0] for element in configuration]
        if self._session:
            return self._session
        # save session data
        self._session = ApplicationSession()
        self._session.add_users(* configuration)

        data= { 'configuration':configuration, 'parameters': parameters}

        # request to server
        data = json.dumps( data)
        response = self._web_session.post(self.url('/sessions'),data=data)

        response_data = json.loads(response.content)
        #self._session.content = response_data
        # save the session data
        if response.status_code >= 200 and response.status_code < 300:
            self._session.current_session(response_data)
            #return True
            return response_data
        else:
            raise RuntimeError('%s' % response.content)


    def close_session(self):
        """
            close current session

        :param session_id:
        :return:
        """
        session_name = self._session.current_session()['session']
        uri = '/sessions/%s' % session_name
        response = self._web_session.delete(self.url(uri))

        self._session.close()
        return response

    def check_session(self,session_id,**kwargs):
        """

        :param kwargs:
        :return:
        """
        data= { 'session_id':session_id, 'parameters': kwargs}

        # request to server
        data = json.dumps( data)
        response = self._web_session.post(self.url('/sessions/%s/check_session' %  session_id),data=data)

        response_data = json.loads(response.content)
        #self._session.content = response_data
        # save the session data
        if response.status_code >= 200 and response.status_code < 300:
            return True
        else:
            raise RuntimeError('%s' % response.content)

    def clear_sessions(self):
        """

        """
        response = self._web_session.post(self.url('/sessions/-/clear_sessions'))

        response_data = json.loads(response.content)
        #self._session.content = response_data
        # save the session data
        if response.status_code >= 200 and response.status_code < 300:
            return True
        else:
            raise RuntimeError('%s' % response.content)




    # def session_command(self,cmd,session_id='-',**kwargs):
    #     """
    #         send a command to session mechanism
    #
    #     :param cmd:
    #     :param kwargs:
    #     :return:
    #     """
    #     data= { 'session_id':'-', 'cmd':cmd  ,'parameters': kwargs}
    #
    #     # request to server
    #     data = json.dumps( data)
    #     response = self._web_session.post(self.url('/sessions/%s/session_command' %  session_id),data=data)
    #
    #     response_data = json.loads(response.content)
    #     #self._session.content = response_data
    #     # save the session data
    #     if response.status_code >= 200 and response.status_code < 300:
    #         return response_data
    #     else:
    #         raise RuntimeError('%s' % response.content)


    def _handle_response(self,response):
        """
            handle the http response

        :param response: flask response object
        :return:  json_data or raise runtime error if http error encountered
        """
        # response analyse
        if response.status_code >= 200 and response.status_code < 300:
            # reponse ok
            try:
                json_data = response.json()
                result = json_data['result']
            except:
                result = None
        else:
            # other http result (404 , 500 )
            raise RuntimeError('failed: status:%s' % str(response.status_code))

        return result


    def __getattr__(self, function_name):
        """

        :param item:
        :return:
        """
        # call a function
        def wrapper(agent_id,*args,**kwargs):
            """

            """
            # compose request
            return self._request(agent_id,function_name,*args,**kwargs)

        return wrapper


    def _request(self,agent_id,operation_name, *args,**kwargs):
        """
            launch a request

        :param agent_id:
        :param args:
        :param kwargs:
        :return:
        """
        session=self._session.current_session()
        try:
            agent_name = session[agent_id]
        except KeyError:
            raise RuntimeError('no such agent [%s] for this session' % agent_id)
        #url =  self._url + "/agents/%s/%s/" % (str(self._session.current_session()[agent_id]), operation_name)
        url =  self._url + "/agents/%s/%s/" % (str(agent_name), operation_name)
        print "POST " + url + " " + str(args) + "  " + str(kwargs)
        response = self._web_session.post(url,data=json.dumps(kwargs))

        # response analyse
        if response.status_code >= 200 and response.status_code < 300:
            # reponse ok
            #response.encoding="utf-8"
            #response.encoding = 'ISO-8859-1'
            try:
                #data= response.text
                #result=json.loads(data)

                json_data = response.json()
                result = json_data['result']
            except:
                result = None
        else:
            # other http result (404 , 500 )
            json_data = response.json()
            # print json_data['message']
            # print json_data['exc_type']
            # print json_data['exc_value']

            text =[]
            text.append(json_data['message'])
            text.append(json_data['exc_type'])
            text.append(json_data['exc_value'])

            raise RuntimeError('failed: status:%s , message: %s' % (str(response.status_code),"  ".join(text)))

        return result




if __name__=="__main__":


    url= "http://localhost:5000"

    users= ['Alice','Bob']

    cnf= [
        { 'agent_name':'userA','options':"--null-audio" },
        { 'agent_name':'userA','options':"--null-audio" }
    ]

    class MyClient(HttpClient):
        """

        """




    def test_basic():


        #client= HttpClient(url=url,agent_factory=agent_factory)
        client= MyClient(url=url)

        configuration= []
        for order,user in enumerate(users):
            configuration.append([user,cnf[order]])

        client.open_session(*configuration)

        res= client.dummy('Alice', p1="v1",p2="v2")

        client.close_session()

        return



    test_basic()

