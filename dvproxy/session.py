
from plugin import PluginMount
from agent import Agent








class SessionBase(object):
    """


    """

    __metaclass__ = PluginMount

    role= 'session'
    version = None


    def __init__(self,parent=None,identifier=None, configuration=None):
        """

        :return:
        """
        # a map of agent instances
        self.parent=parent
        self.id=identifier
        self._agents = {}
        self.configuration=configuration or []

    def open(self,configuration=None):
        """
            init session in creating agents

        :param configurations: list , list of parameters for creating agents
        :return:

        eg    [

                { id1, factory_name, parameters },
                { id2, factory_name , parameters },
            ]

        """
        configuration = configuration or self.configuration
        self.configuration=configuration

        rc= self.lock_agents(configuration)
        if rc:
            # lock succesfull
            # start an agent for each item of config
            for conf in configuration:
                # each configuration is a tuple ( agent_id , parameters )
                identifier, factory_name, parameters= conf

                factory= Agent.select_plugin('agent',factory_name)

                agent = factory(identifier, **parameters)

                self._agents[identifier] = agent

            return self.setup(self.configuration)
        else:
            # lock agents has failed
            raise RuntimeError("Session canceled: one of the agent is locked")

    def lock_agents(self,configuration):
        """
            ask the hub (if any to lock the agents )

        :param configuration: list of dict ( agent_id,category,parameters)
        :return:
        """
        failed =False
        if self.parent:
            # there is a hub to ask for
            acquired= []
            for agent_id,category,parameters in configuration:
                factory= Agent.select_plugin('agent',category)
                lock_id= factory.guess_agent_id(agent_id,**parameters)
                rc= self.parent.lock_agent(lock_id)
                if rc == True:
                    # agent is locked, mark it
                    acquired.append(agent_id)
                else:
                    failed= True
                    break
            if failed:
                # at least one agent not available : release the locks
                self.release_agents(*acquired)
                return False
            else:
                # ok
                return True
        else:
            # no hub , assume it is ok
            return True

    def release_agents(self,*agents):
        """

        :param agent: list of agent_id
        :return:
        """
        if self.parent:
            for agent_id in agents:
                self.parent.release_agent(agent_id)


    def close(self):
        """

            close session:
                call agent.close() for each agents
                del agents list
                release agent lock for each agent_id
        :return:
        """
        # all agents of the session
        agents= [e[0] for e in self.configuration]

        # close all agents
        agent_closed= 0
        for agent in self._agents.values():
            agent.close()
            agent_closed += 1
        # delete agent pointer
        for agent_id in self._agents.keys():
            del self._agents[agent_id]
        self._agents={}

        # release lock for each agent
        self.release_agents(*agents)
        return agent_closed


    def agent(self,identifier):
        """
            return an agent object by id

        :param identifer:
        :return:
        """
        try:
            return self._agents[identifier]
        except KeyError:
            raise KeyError('so such agent: [%s]' % identifier)

    def setup(self,configuration):
        """
        customisation for open a hub session
        :return:
        """
        return True


    def __getattr__(self, function_name):
        """
            wrapper to Mobile methods

        :param function_name:
        :return:
        """
        # call a function
        def wrapper(agent_id,*args,**kwargs):
            """

            """
            agent = self.agent(agent_id)
            # intercept timeout arguments if any
            if 'timeout' in kwargs:
                # convert timeout to int
                kwargs['timeout'] = int(kwargs['timeout'])
            try:
                function = getattr(agent,function_name)
            except AttributeError,e:
                raise e
            return function(*args,**kwargs)
        return wrapper


class Session(SessionBase):
    """

    """