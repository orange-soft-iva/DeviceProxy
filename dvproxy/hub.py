__author__ = 'cocoon'
"""

    a Hub Api

        HubApi( agent_factory )

        open_session()
        close_session()

        get_agent(agent_id)

        __getattr__ : agent proxy


        ..run


    an Agent Api

        AgentApi ( identifier , parameters )
        setup()
        dummy()

"""
import random
import threading
import copy

from session import Session
from agent import Agent

import logging
log= logging.getLogger(__name__)



class AgentLocks(object):
    """
        lock systems for agents

    """
    def __init__(self,agents=None):
        """

        """
        self._agents= agents or {}
        assert isinstance(self._agents,dict)
        self._lock= threading.Lock()

    def lock(self,agent_id,**kwargs):
        """

        """
        #log.debug('AgentLocks.lock wait for a lock')
        acquired= False
        self._lock.acquire()
        try:
            #log.debug('AgentLocks.lock acquired a lock')
            if not agent_id in self._agents:
                self._agents[agent_id]= kwargs
                log.debug("AgentLocks: agent [%s] is now marked busy" % agent_id)
                acquired= True
        finally:
            #log.debug('AgentLocks.lock release a lock')
            self._lock.release()
        return acquired

    def release(self,agent_id):
        """

        """
        #log.debug('AgentLocks.unlock wait for a lock')
        released= False
        self._lock.acquire()
        try:
            #log.debug('AgentLocks.unlock acquired a lock')
            if agent_id in self._agents:
                del self._agents[agent_id]
                log.debug("AgentLocks: agent [%s] is now marked free" % agent_id)
                released= True
        finally:
            #log.debug('AgentLocks.unlock release a lock')
            self._lock.release()
        return released

    def get_locked_agents(self):
        """

        """
        state={}
        log.debug('AgentLocks.get_locked_agents wait for a lock')
        self._lock.acquire()
        try:
            state= copy.deepcopy(self._agents)
        finally:
            log.debug('AgentLocks.get_locked_agents release a lock')
            self._lock.release()
        return state


class Hub(object):
    """

        a hub to manage sessions , this is the entry point to create sessions


        Hub(**parameters)

        open_session(configuration)
        close_session()

        agent(agent, session) get agent of a session


    """
    session_type= None

    allow_session_reopen= True
    disable_agent_locks= False

    def __init__(self,**parameters):
        """

        :return:
        """
        # a map of agent instances
        self._sessions={}
        self._agents = {}
        self.parameters=parameters
        self._update_parameters(parameters)
        self.locker=AgentLocks()

        self.setup()


    def setup(self,create=False):
        """

        :param create:
        :return:
        """
        return True


    def _update_parameters(self,parameters):
        """

        :param parameters:
        :return:
        """
        for k,v in parameters.iteritems():
            try:
                getattr(self,k)
                setattr(self,k,v)
            except AttributeError:
                pass

    def open_session(self,configuration,session_id='default',session_type=None,**kwargs):
        """
            init session in creating agents

        :param configurations: list , list of parameters for creating agents
        :return:

        eg    [

                { id1, factory_name, parameters },
                { id2, factory_name , parameters },
            ]

        """
        session_type=session_type or self.session_type

        if session_id in self._sessions:
            if self.allow_session_reopen:
                log.warning('last session [%s] left unclose: close it.' % session_id)
                self.session(session_id).close()
            else:
                msg= 'session [%s] is already in use, close it' % session_id
                log.error(msg)
                raise RuntimeError(msg)
        # create session

        # set me to None to disable lock agents
        if self.disable_agent_locks :
            me=None
        else:
            me=self

        # create session
        Session_factory= Session.select_plugin('session',version=session_type)
        session= Session_factory(me,identifier=session_id)
        #session= Session(me,identifier=session_id)
        #open it
        rc= session.open(configuration=configuration)
        # register it
        self._sessions[session_id] = session
        return rc

    def close_session(self,session_id='default'):
        """

        :return:
        """
        r= self.session(session_id).close()
        del self._sessions[session_id]
        return r

    def check_session(self,session_id='default'):
        """

        :param session_id:
        :return:
        """
        session= self.session(session_id)
        return {'session.id':session_id}


    def clear_sessions(self,**kwargs):
        """

        :param session_id:
        :return:
        """
        session_list=[]
        for session in self._iter_session():
            session_list.append(session.id)
            session.close()
        return session_list

    def session(self,session_id='default'):
        """

            :return the session by id

        :param session_id:
        :return:
        """
        try:
            return self._sessions[session_id]
        except KeyError:
            raise KeyError('no such session: %s' % session_id)

    def agent(self,agent_id,session_id='default'):
        """
            return an agent object by id in a specific section

        :param identifer:
        :return:
        """
        return self.session(session_id).agent(agent_id)

    def setup_session(self,configuration,session_id='default'):
        """
        customisation for open a hub session
        :return:
        """
        return self.session(session_id).setup(configuration)



    # mapping for the agent method ( click , wait_for_exists , press_home ... )
    def __getattr__(self, function_name):
        """
            wrapper to Mobile methods

        :param function_name:
        :return:
        """
        # if function_name.startswith('_'):            #in ['__nonzero__','__call__' ]:
        #      return self
        # call a function
        def wrapper(agent_id,*args,**kwargs):
            """

            """
            session_id= kwargs.get('session_id','default')
            agent = self.agent(agent_id,session_id=session_id)
            # intercept timeout arguments if any
            if 'timeout' in kwargs:
                # convert timeout to int
                kwargs['timeout'] = int(kwargs['timeout'])
            try:
                function = getattr(agent,function_name)
            except AttributeError,e:
                raise e
            return function(*args,**kwargs)
        return wrapper


    # reserved
    def lock_agent(self,agent_id,**data):
        """
            return True if lock is obtained
            or false if agent is busy
        """
        return self.locker.lock(agent_id,**data)


    def release_agent(self,agent_id):
        """

        """
        return self.locker.release(agent_id)


    @classmethod
    def random_session_id(cls):
        """
            return a random session

        :return:
        """
        r = "%06x" % random.randint(0,256**3-1)
        return r


    def session_factory(self,version=None):
        """
            return the session factory

        :param version:
        :return:
        """
        version= version or self.session_type
        return Session.select_plugin('session',version=version)


    def _iter_session(self):
        """

        :return:
        """
        for session in self._sessions.values():
            yield session


if __name__=="__main__" :


    logging.basicConfig(level=logging.DEBUG)

    session_configuration=    [
                [ 'id1', 'one',  {} ],
                [ 'id2', 'two' , {} ],
            ]



    class AgentOne(Agent):
        """

        """
        version= 'one'


        def dummy(self,**kwargs):
            print "Agent %s of type [one]: dummy(%s)" % (self.identifier,str(kwargs))


    class AgentTwo(Agent):
        """

        """
        version= 'two'


        def dummy(self,**kwargs):
            print "Agent %s of type [Two]: dummy(%s)" % (self.identifier,str(kwargs))


    #

    def test_session():

        s= Session()

        s.open(session_configuration)

        agent_factory_1= Agent.select_plugin('agent','one')
        agent_factory_2= Agent.select_plugin('agent','two')
        #
        #
        a1= agent_factory_1('id1')
        a2= agent_factory_2('id2')

        #s.agent('id1').dummy()
        #s.agent('id2').dummy()

        s.dummy('id1')
        s.dummy('id2')




        try:
            s.agent('id3').dummy()
        except KeyError:
            pass

        return


    def test_hub():

        h= Hub()

        s= h.open_session(session_configuration)

        #h.agent('id1').dummy()
        #h.agent('id2').dummy()


        print h.dummy('id1')
        print h.dummy('id2', p='p1')


        h.close_session()


        s= h.open_session(session_configuration)
        # reopen it
        s= h.open_session(session_configuration)

        h.close_session()



    def test_hub_locks():
        """

        :return:
        """
        h= Hub()
        s1= h.open_session(session_configuration)

        # try to open a session with same agents
        try:
            s2= h.open_session(session_configuration)
        except RuntimeError:
            pass

        h.close_session()

        return




    test_session()
    test_hub()
    test_hub_locks()
    print 'Done'


