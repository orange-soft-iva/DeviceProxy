__author__ = 'cocoon'
"""

    an http proxy for agents


    see server.py as a sample usage




"""
import random
import json

from facets.utils.store import Store as BaseStore


# for facets
from flask import request
from facets import get_application
from facets import CollectionWithOperationApi

#from facets import InvalidUsage, ApplicationError

class Store(BaseStore):
    """

    """
    collections = [ 'main','sessions', 'devices', 'users' ]


db = Store()
#api= NativeClient()

# get a flask application
app = get_application(__name__,with_error_handler=True)



@app.route('/')
def index():
    """

    :return:
    """
    return "hello from sypslave server"



class Agents(CollectionWithOperationApi):
    """
            manage agents /agents

    """
    def agent_to_user_session(self,agent_name):
        """
            convert agent_name to username and session Alice-123  (Alice,123)

        :param agent_name str: the agent name eg Alice-1234567

        """
        return agent_name.split('-')


    def operation(self,**kwargs):
        """

            dispatch operation of agents

            /agents/<item>/<operation>


        :param operation str: operation name eg : answer_call ...
        :param item_id str: agent name eg: Alice ,Bob ...
        :return:
        """
        # it is an operation

        operation = kwargs['operation']

        r = request
        r.encoding='utf-8'
        data = r.json
        agent = kwargs['item']
        user,session_id = self.agent_to_user_session(agent)


        # get session
        session= self.api.session(session_id)
        try:
            method = getattr(session,operation)
        except AttributeError,e:
            raise e


        # if not session_id== "*":
        #     # check existing session
        #     session=db.get('sessions',session_id)
        #
        # try:
        #     method = getattr(self.api,operation)
        # except AttributeError,e:
        #     raise e

        # call agent method  (mobile)  eg press_home , click()
        try:
            rs = method(user,**data)
        except:
            raise

        result = dict( result= rs , text = "result: %s , operation: %s " % (rs , operation))
        #return "result: %s , operation: %s " % (str(rs) , operation)
        return json.dumps(result)


    # @classmethod
    # def set_api(cls, api):
    #     """
    #
    #     :param api: instance of Native agent api
    #     :return:
    #     """
    #     cls.api=api



class HubSessions(CollectionWithOperationApi):
    """
        manage sessions

        /sessions

    """
    def get(self,item = None):
        return "list session"

    def post(self,*args,**kwargs):
        """
            create a session
        """
        return self.op_open_session(*args,**kwargs)

    def delete(self,item=None):
        """
            delete session
        """
        return self.op_close_session(item=item)


    def op_open_session(self,*args,**kwargs):
        """
            open session
        """
         # create a dummy session
        r =request
        data = request.json

        configuration= data['configuration']
        parameters=data['parameters']


        session_id=self.api.random_session_id()


        ### CALL the open session
        self.api.open_session(configuration,session_id=session_id,**parameters)


        # create response the session id and the agent ids
        response = { 'session': session_id }
        for user_data in configuration:
            agent_id, agent_type,parameters= user_data
            agent_name = "%s-%s" %( agent_id,session_id)
            response[agent_id]= agent_name


        #s = { 'session': session_id, 'Alice': 'Alice-123' , 'Bob': 'Bob-123' }
        return json.dumps(response)

    def op_close_session(self,**kwargs):
        """
            close session
        """
        session_id = kwargs['item']

        ### close the CALL session
        #relay.close_session()
        self.api.close_session(session_id=session_id)


        #db.delete('sessions',session_id)
        return "session closed"


    def op_check_session(self,**kwargs):
        """


        :param kwargs:
        :return:
        """
        session_id= kwargs['item']
        r= self.api.check_session(session_id=session_id)
        return json.dumps(r)

    def op_clear_sessions(self,**kwargs):
        """
            clear all sessions to unlock devices

        """
        r = self.api.clear_sessions()
        return json.dumps(r)



    # def op_session_command(self,**kwargs):
    #     """
    #
    #     :param session_id:
    #     :param kwargs:
    #     :return:
    #     """
    #     session_id= kwargs['item']
    #     data= request.json
    #
    #     if session_id== '-':
    #         # generic operation on all sessions
    #         session_manager_factory= self.api.session_factory()
    #         result= session_manager_factory.session_command(data['cmd'])
    #
    #     else :
    #         # operation on session $session_id
    #         raise NotImplementedError
    #
    #
    #     return json.dumps(result)


    def operation(self,**kwargs):
        """

            dispatch operation

        :param operation:
        :param item_id:
        :return:
        """
        # it is an operation

        operation = kwargs['operation']
        session_id= kwargs['item']

        func_name = "op_%s" % operation
        if hasattr(self, func_name):
            m =  getattr(self,func_name)

            # method implemented ( op_<operation>  )
            return m(**kwargs)
        else:
            # method not implemented directly, see if api define it for session in session factory
            session_factory= self.api.session_factory()
            if hasattr(session_factory,operation):
                m = getattr(session_factory,operation)
                # run the method of the api
                result= m(**kwargs)
                return json.dumps(result)

            else:
                raise NotImplementedError('Bad method (%s) for operation: %s' % (request.method,operation))



def create_proxy(hub_api,sessions='sessions',agents='agents',sessions_url=None,agents_url=None):
    """

    :param hub_api:
    :param sessions:
    :param agents:
    :param sessions_url:
    :param agents_url:
    :return:
    """
    sessions_url= sessions_url or "/%s" % sessions
    agents_url= agents_url or "/%s" % agents

    # customize api
    Agents.set_api(hub_api)
    HubSessions.set_api(hub_api)


    # create and register collections : sessions , agents ..
    sessions = HubSessions.create( app ,sessions, sessions_url )
    agents = Agents.create(app,agents,agents_url)

    return sessions,agents



# create and register collections : sessions , agents ..
#sessions = HubSessions.create( app ,'sessions', '/sessions' )
#agents = Agents.create(app,'agents','/agents')

#Agents.set_api(api)


def start(host="0.0.0.0" , port=5000 , debug=True):
    """

    :return:
    """

    app.debug = debug

    #app.run(host="127.0.0.1",port = 5001)
    app.run(host=host,port = port)





# if __name__=="__main__":
#
#
#
#     #from hub import NativeClient
#     from hub import Hub as HubApi
#     from dvproxy.samples_orig.agent import Agent as AgentApi
#
#     from dvproxy.samples
#
#
#     # test
#     def test_random():
#
#
#         for i in xrange(0,5):
#
#             print HubSessions.random()
#
#     def test():
#
#
#         api= HubApi(AgentApi)
#
#         Agents.set_api(api)
#         HubSessions.set_api(api)
#
#
#         with app.test_request_context():
#
#             print url_for('index')
#
#         # start the server
#         start()
#
#
#     # begin here
#     test()