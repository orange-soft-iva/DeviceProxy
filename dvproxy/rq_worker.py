import os
import time
import redis
from rq import Worker, Queue, Connection

# import delegation
from rq.job import Job

from rq.exceptions import NoSuchJobError


# for shell commands
import subprocess
from subprocess import PIPE,STDOUT

# adapters
from adapter import AdapterBase

import logging
log=logging.getLogger(__name__)



listen = ['default']

redis_url = os.getenv('REDISTOGO_URL', 'redis://localhost:6379')

conn = redis.from_url(redis_url)

#
#
#
# #
# # convenient functions
#
# def get_queue():
#     """
#
#     :return:
#     """
#     q = Queue(connection=conn)
#     return q
#
#
# def fetch_job(job_key):
#     """
#
#     :param job_key:
#     :return:
#     """
#     try:
#         job = Job.fetch(job_key, connection=conn)
#     except NoSuchJobError:
#         job= None
#     return job




#
# context
#


class RqAdapter(AdapterBase):
    """
        Base class for Adapter, ClientAdapter

    """
    # key_prefix= "/agents"
    # cmd_exit= 'exit\n'
    #
    # redis_kwargs={}
    # agent_id= None
    # trace_enabled= True
    # db=None
    #
    # Terminated=False
    #
    #
    # def trace(self, s):
    #     if self.trace_enabled:
    #         #now = time.time()
    #         #fmt = "================== " + s + " ==================" + " [at t=%(time)03d]"
    #         #self.log(fmt % {'time': int(now - self.t0)})
    #         print s
    #
    #
    # def queue_name(self,name):
    #     """
    #
    #     """
    #     return '%s/%s/%s' % (self.key_prefix, self.agent_id,name)
    #
    #
    # def status(self):
    #     """
    #         return the contents of hash /agents[agent_id]
    #     """
    #     stat= self.db.hget(self.key_prefix,self.agent_id)
    #     return stat
    #
    # def agents_status(self):
    #     """
    #         list status of  all declared agents
    #
    #     :return:
    #     """
    #     return self.db.hgetall(self.key_prefix)
    #
    # def stop(self):
    #     """
    #
    #     :return:
    #     """
    #     self.Terminated= True

    def __init__(self,agent_id,key_prefix='/agents',**redis_kwargs):
        """

        """
        self.agent_id=agent_id
        self.redis_kwargs= redis_kwargs
        if 'db' in redis_kwargs.keys:
            self.db= redis_kwargs['db']
        else:
            self.db= redis.Redis(** self.redis_kwargs)
        self.key_prefix= key_prefix

        # create rq queue
        self.q= Queue(connection=self.db)

        #self.q_in = RedisQueue(self.queue_name('stdin'),**self.redis_kwargs)
        #self.q_out= RedisQueue(self.queue_name('stdout'),**self.redis_kwargs)


    def send(self,cmd,**kwargs):
        """
            send command to redis queue

            cmd : python callable

            return the job id

        """

        # set agent result to None
        self._set_job_result(None)


        # enqueue main job
        job= self.q.enqueue(cmd,**kwargs)
        job_id= job.id

        # store job id
        self.db.set(self.queue_name('job'), str(job_id) )


        # launch update worker
        self.q.enqueue(wk_update_job_result(agent_id=self.agent_id,redis_db=self.db,timeout=30))

        return job.id


    def _read(self):
        """
            return the job result ( None if not finished )

        """
        # fetch the job_id
        job_id= self.db.get(self.queue_name('job'))

        # ask rq for status
        job = self.q.fetch_job(job_id)


        # update agent result with job result  (/agents/<agent_id>)
        self._set_job_result(job.result)

        # return the result , None if job is not finished
        return job.result


    def read(self,timeout=1):
        """
            read result from worker
        """
        if timeout > 0 :
            job_result= self.wait(counter= timeout )
        else:
            job_result= self._read()

        return job_result


    def keep_alive(self):
        """
            return the timestamp of agent keep alive
        """
        raise NotImplementedError
        #return self.db.hget(self.key_prefix,self.agent_id)

    ###
    def exit(self):
        """
            send exit to agent adapter
        """
        raise NotImplementedError
        #self.send(self.cmd_exit)


    def wait(self,counter= 2):
        """
            wait until adapter is done (eg keep alive is None)
        """
        result= None

        while result is None:

            result= self._read()

            if result is None:
                # job is not finished
                if counter <= 0:
                    # timeout , force leaving
                    break
                # wait and retry
                counter= counter -1
                time.sleep(1)
            # result is available, break loop and return result
        return result


    def _set_job_result(self,result):
        """

        set /agents/<agent_id> with  { 'result': result }

        :param selfresult:
        :return:
        """
        if not isinstance(result,dict):
            result= {'result':result}
        self.db.hset( "%s/%s" % (self.key_prefix,self.agent_id) , result)
        return




#
#  JOBS
#

def wk_update_job_result(agent_id,redis_db=None,timeout=1):
    """

        a worker to watch job result and update /agents/$id status

    :param agent_id:
    :param timeout:
    :return:
    """
    adapter= RqAdapter(agent_id,db=redis_db)

    result= adapter.read(timeout=timeout)

    return True





if __name__ == '__main__':
    with Connection(conn):
        worker = Worker(list(map(Queue, listen)))
        worker.work()

