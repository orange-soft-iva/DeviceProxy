#!/usr/bin/env python

from setuptools import setup
from dvproxy import __version__


setup(
    name= 'phone_hub',
    version= __version__,
    long_description = '''''',
    author = "Laurent Tordjman",
    author_email = "laurent.tordjman@orange.com",
    license = '',
    url = 'https://gitlab.com/orange-soft-iva/DeviceProxy.git',
    py_modules= ['phone_hub'],
    install_requires= [
        'dvproxy', 'facets'
    ],
    entry_points='''
        [console_scripts]
        adapter=dvproxy.adapter:cli
    ''',
)