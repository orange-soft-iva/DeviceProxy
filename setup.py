#!/usr/bin/env python

from setuptools import setup
from dvproxy import __version__


import sys

all = False
if '--all' in sys.argv:
    index = sys.argv.index('--all')
    sys.argv.pop(index)  # Removes the '--foo'
    #foo = sys.argv.pop(index)  # Returns the element after the '--foo'
    all=True
# The foo is now ready to use for the setup




setup(
    name= 'DeviceProxy',
    version= __version__,
    long_description = '''''',
    author = "Laurent Tordjman",
    author_email = "laurent.tordjman@orange.com",
    license = '',
    url = 'https://gitlab.com/orange-soft-iva/DeviceProxy.git',
    py_modules= ['dvproxy','facets'],
    install_requires= [
        'click','flask','requests'
    ],
    entry_points='''
        [console_scripts]
        adapter=dvproxy.adapter:cli
    ''',
)