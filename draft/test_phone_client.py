__author__ = 'cocoon'


import time
import json

from dvclient.client import Client
from dvclient.agent import Agent
from dvclient.session import Session




class AndroidAgent(Agent):
    """


    """
    version='android_device'



    def press_home(self):
        """


        :return:
        """
        return self._request(self._agent_name,'press_home')



    def remote_dummy(self):
        """


        :return:
        """
        print "android device remote dummy"


class PjsipAgent(Agent):
    """


    """
    version='pjsip_device'



    def remote_dummy(self):
        """


        :return:
        """
        print "pjsip device remote dummy"



class PhoneSession(Session):
    """


    """
    version='phone'






class PhoneClient(Client):
    """


    """
    session_type = 'phone'

    def adb_devices(self,**kwargs):
        """
            list of android devices  ( /sessions/-/adb_devices )
        :return:
        """
        data= { 'session_id':'-', 'cmd':'devices'  ,'parameters': kwargs}

        # request to server
        data = json.dumps( data)
        response = self._web_session.post(self.get_url('/sessions/-/adb_devices' ),data=data)

        #response = self._web_session.post(self.url('/agents/-/adb_devices' ),data=data)


        response_data = json.loads(response.content)
        #self._session.content = response_data
        # save the session data
        if response.status_code >= 200 and response.status_code < 300:
            return response_data
        else:
            raise RuntimeError('%s' % response.content)



if __name__=="__main__":


    url= "http://localhost:5000"


    pjsip_parameters= {
        'platform': "../samples/platform_demo.json",
        'platform_name': 'demo',
        'platform_version': "demo_qualif"
    }

    Alice= 'Alice'
    Bob= '0a9b2e63'

    #Charlie= '127.0.0.1:5555'

    Charlie= 'Charlie'

    users= ['Alice',Bob]

    #         identifier type         parameters

    # to enable charlie :  start bluestack android emulator then "adb connect 127.0.0.1:5555"


    cnf= [
            [ Alice,    'pjsip_device',    {'command_line':""}         ],
            [ Bob  ,    'android_device', {}         ],
            [ Charlie  ,    'pjsip_device',  {'command_line':""}          ],


            #[ Charlie  ,    'droydterm', {}         ],
        ]




    def test_basic():


        #client= HttpClient(url=url,agent_factory=agent_factory)

        client= PhoneClient(url=url)

        client.clear_sessions()
        client.open_session(cnf)

        client.dummy(Alice)
        #client.dummy(Bob)
        #client.dummy(Charlie)

        try:
            client.call_literal(Alice)
        except RuntimeError:
            pass


        a= client.remote_dummy(Alice)
        b= client.remote_dummy(Bob)
        c= client.remote_dummy(Charlie)

        #devices = client.adb_devices()

        home= client.press_home(Bob)


        time.sleep(2)
        print " read Alice"
        lines= client.read_stdout('Alice')
        print lines

        print "dump_status on Alice"
        client.dump_status('Alice')
        time.sleep(2)
        lines= client.read_stdout('Alice')
        print lines

        print "dump detailed status on Alice"
        client.write_stdin('Alice', message='dd\n')
        time.sleep(2)
        lines= client.read_stdout('Alice')
        print lines


        androis_devices= client.adb_devices()

        print androis_devices


        client.close_session()

        print client



    test_basic()
    pass








    print

