__author__ = 'cocoon'

import json
from dvclient.client import Client,ApplicationSession
from pjsip_device.session import PjsipSessionConfiguration

#
# import plugins
#
from session import Session
from  android_device import AndroidAgent
from pjsip_device import PjsipAgent






class PhoneClient(Client):
    """


    """
    session_type = 'phone'


    def adb_devices(self,**kwargs):
        """
            list of android devices  ( /sessions/-/adb_devices )
        :return:
        """
        data= { 'session_id':'-', 'cmd':'devices'  ,'parameters': kwargs}

        # request to server
        data = json.dumps( data)
        response = self._web_session.post(self.get_url('/sessions/-/adb_devices' ),data=data)

        #response = self._web_session.post(self.url('/agents/-/adb_devices' ),data=data)


        response_data = json.loads(response.content)
        #self._session.content = response_data
        # save the session data
        if response.status_code >= 200 and response.status_code < 300:
            return response_data
        else:
            raise RuntimeError('%s' % response.content)


    def open_session(self,configuration,**parameters):
        """

            open a session with configuration
                store accounts

         :param configuration:list, configuration of the session list of dict name,type,paramameters
         :param parameters: dict : parameters for the session type

         example of parameters: session_type, session_id ...
        :return:
        """
        if self._session:
            return self._session


        # setup configuration for pjsip
        cnf= PjsipSessionConfiguration(configuration)

        # setup pjsip configuration ( command_line)
        configuration= cnf.setup(self.parameters)




        #
        # initialise a remote web session
        #

        # save session data
        self._session = ApplicationSession()
        self._session.add_users(* configuration)

        data= { 'configuration':configuration, 'parameters': parameters}

        # request to server
        data = json.dumps( data)
        url= self.get_url('/sessions')
        response = self._web_session.post(url,data=data)

        response_data = json.loads(response.content)
        # save the session data
        if response.status_code >= 200 and response.status_code < 300:
            self._session.current_session(response_data)
            #return True
            #return response_data
        else:
            raise RuntimeError('%s' % response.content)


        #
        # initialize local session
        #
        factory_name= self.session_type
        factory= Session.select_plugin('session',version=factory_name)
        # create session
        session=factory(self)
        session.open(configuration=configuration)





        # store session object
        self._sessions['default']=session

        return



if __name__=="__main__":


    import time

    url= "http://localhost:5000"


    pjsip_parameters= {
        'platform': "../samples/platform_demo.json",
        'platform_name': 'demo',
        'platform_version': "demo_qualif"
    }

    Alice= 'Alice'
    Bob= '0a9b2e63'

    #Charlie= '127.0.0.1:5555'

    Charlie= 'Charlie'

    users= ['Alice',Bob]

    #         identifier type         parameters

    # to enable charlie :  start bluestack android emulator then "adb connect 127.0.0.1:5555"


    cnf= [
            [ Alice,    'pjsip_device',    {}         ],
            [ Bob  ,    'android_device', {}         ],
            [ Charlie  ,    'pjsip_device',  {}          ],


            #[ Charlie  ,    'droydterm', {}         ],
        ]




    def test_basic():


        #client= HttpClient(url=url,agent_factory=agent_factory)

        client= PhoneClient(url=url,**pjsip_parameters)

        client.clear_sessions()
        client.open_session(cnf)

        client.dummy(Alice)
        #client.dummy(Bob)
        #client.dummy(Charlie)

        try:
            client.call_literal(Alice)
        except RuntimeError:
            pass


        a= client.remote_dummy(Alice)
        b= client.remote_dummy(Bob)
        c= client.remote_dummy(Charlie)

        #devices = client.adb_devices()

        home= client.press_home(Bob)


        time.sleep(2)
        print " read Alice"
        lines= client.read_stdout('Alice')
        print lines

        # print "dump_status on Alice"
        # client.dump_status('Alice')
        # time.sleep(2)
        # lines= client.read_stdout('Alice')
        # print lines



        print "dump detailed status on Alice"


        #client.write_stdin('Alice', message='dd\n')
        client.send('Alice', cmd='dd')
        client.expect('Alice', pattern='Dump Complete')


        time.sleep(2)
        lines= client.read_stdout('Alice')
        print lines

        client.shutdown('Alice')


        android_devices= client.adb_devices()

        print android_devices


        client.close_session()

        print client



    test_basic()
    pass








    print
