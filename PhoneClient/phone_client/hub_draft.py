

from dvproxy.hub import Hub
from dvproxy.session import Session
from phone_cli import HttpClient



# import to declare plugins
from android_device.session import AndroidSession
from pjsip_device.session import PjsipSession
from android_device.agent import AndroidAgent
from pjsip_device.agent import PjsipAgent

class ClientHub(Hub):
    """

        a hub for client sessions


            open and setup a session for each type of agents ( android,pjsip)

            open a main session in remote


        open_session(self,configuration,session_id='default',session_type=None,**kwargs)

        close_session(self,session_id='default')
        check_session(self,session_id='default')
        clear_sessions(self,**kwargs)


        setup_session(self,configuration,session_id='default')

        """

    # def __init__(self,**parameters):
    #     """
    #
    #     :return:
    #     """
    #     # a map of agent instances
    #     self._sessions={}
    #     self._agents = {}
    #     self.parameters=parameters
    #     self._update_parameters(parameters)
    #     self.locker=AgentLocks()

    def open_session(self,configuration,session_id='default',session_type=None,**kwargs):
        """
            analyse configuration

            make session for each type of terminals ( android_session, pjsip_session)

        :param configurations: list , list of parameters for creating agents
        :return:

        eg    [

                { id1, factory_name, parameters },
                { id2, factory_name , parameters },
            ]

        """
        self.agent_categories={}
        self.configuration= configuration
        # analyse sessions
        session_analyse={}
        for line in self.configuration:
            identifier,factory_name,parameter= line
            if not factory_name in session_analyse:
                # create entry for this factory name
                session_analyse[factory_name]=[]
            #
            session_analyse[factory_name].append(line)

            # memo for category of agent
            self.agent_categories[identifier]= factory_name

        # create a session for each kind
        for factory_name,cnf in session_analyse.iteritems():
            factory= Session.select_plugin('session',version=factory_name)
            # create session
            session=factory(self,factory_name,cnf)

            # store session
            self._sessions[factory_name]= session

            # setup session
            session.setup(cnf)
            #self.setup_session(configuration,factory_name)


        # open each kind of session ( eg create client agents
        for session in self._sessions.values():
            session.open()

        return

    def setup_session(self,configuration,session_id='default'):
        """
        customisation for open a hub session
        :return:
        """
        #return True
        #for session_id,configuration in self._sessions.iteritems():
        #    session.setup()
        return self.session(session_id).setup(configuration)



    # mapping for the agent method ( click , wait_for_exists , press_home ... )
    def __getattr__(self, function_name):
        """
            wrapper to Mobile methods

        :param function_name:
        :return:
        """
        # call a function
        def wrapper(agent_id,*args,**kwargs):
            """

            """
            session_id= kwargs.get('session_id','default')
            agent = self.agent(agent_id,session_id=session_id)
            # intercept timeout arguments if any
            if 'timeout' in kwargs:
                # convert timeout to int
                kwargs['timeout'] = int(kwargs['timeout'])
            try:
                function = getattr(agent,function_name)
            except AttributeError,e:
                raise e
            return function(*args,**kwargs)
        return wrapper

    def session(self,session_id='default'):
        """

            :return the session by id

        :param session_id:
        :return:
        """
        try:
            return self._sessions[session_id]
        except KeyError:
            raise KeyError('no such session: %s' % session_id)

    def agent(self,agent_id,session_id='default'):
        """
            return an agent object by id in a specific section

        :param identifer:
        :return:
        """
        # what category for this agent
        category= self.agent_categories[agent_id]

        return self.session(category).agent(agent_id)



class Main(object):
    """



    """
    def __init__(self,url, **parameters):
        """


        :param kwargs:
        :return:
        """
        self.http_client= HttpClient(url)
        self.hub= ClientHub(**parameters)




if __name__=="__main__":



    url= "http://localhost:5000"


    pjsip_parameters= {
        'platform': "../samples/platform_demo.json",
        'platform_name': 'demo',
        'platform_version': "demo_qualif"
    }

    Alice= 'Alice'
    Bob= '0a9b2e63'

    #Charlie= '127.0.0.1:5555'

    Charlie= 'Charlie'

    users= ['Alice',Bob]

    #         identifier type         parameters

    # to enable charlie :  start bluestack android emulator then "adb connect 127.0.0.1:5555"


    cnf= [
            [ Alice,    'pjsip_device',   { }         ],
            [ Bob  ,    'android_device', {}         ],
            [ Charlie  ,    'pjsip_device', {}         ],


            #[ Charlie  ,    'droydterm', {}         ],
        ]




    def test_basic():


        #client= HttpClient(url=url,agent_factory=agent_factory)
        client= Main(url=url, ** pjsip_parameters)


        client.hub.open_session(cnf)

        a= client.hub.agent('Alice')


        print client



    test_basic()
    pass