__author__ = 'cocoon'
"""


    manager for platform  config



    based on a structure ( like ptf.yaml)


    provides services

        resolve_user_destination( user , format )

        get_user_config( user )


        resolve_fac_redirection_activate( user , format , fac_kind )
        resolve_fac_redirection_deactivate( fac_kind )


        get_destination ( key ,format)




"""
import os
from copy import deepcopy
from phoneparser import FrenchPhoneNumber

import json

# conditional import of yaml module
yaml_module= True
try:
    from yaml import load,dump
except ImportError:
    yaml_module= False

if yaml_module:
    try:
        from yaml import CLoader as Loader, CDumper as Dumper
    except ImportError:
        from yaml import Loader, Dumper



class User():
    """
        handle user (Alice , Bob)


        functions:

        to_user(format)
        # compute the number to join this user in a particuliar forrmat (ext,universal...)


        data samples :

        [Alice]
        LocationDialingCode: 11
        display: Alice
        domain: bluebox
        password: cocoon
        username: +33146502510

        [Alice_noSDA]
        LocationDialingCode: 11
        display: Alice_noSDA
        domain: bluebox
        number: 6310
        password: cocoon
        username: P2361EVQ1_6310

        [Bob_Abbreviated]
        display: Bob_Abbreviated
        domain: bluebox
        password: cocoon
        target: Bob_Abbreviated_Target
        username: 3212
        virtual: true


        [Bob_noSDA_Short]
        LocationDialingCode: 11
        display: Bob_noSDA_Short
        domain: bluebox
        number: 6312
        password: cocoon
        target: Bob_Short_Target
        username: P2361EVQ1_6312

    """

    escape="0"
    number_formats=['international','national','escape_international','escape_national','sid_ext','ext','contact','universal']

    def __init__(self,data,escape_prefix="",escape_prefix_explicit="0"):
        """
            @data: dict (username,domain,contact, site
        """
        self.data=data
        self.escape_prefix=escape_prefix
        self.escape_prefix_explicit=escape_prefix_explicit
        if self.escape_prefix and not self.escape_prefix_explicit:
            # when prefix is set and no explicit prefix: use prefix as value for explicit prefix
            self.escape_prefix_explicit = self.escape_prefix

        # in case user has an explicit public name take it as username
        if data.has_key('id'):
            # username is the id (public
            self.username= self.data['id']
        else:
            # username is already the public
            self.username=self.data['username']
        self.password=self.data['password']
        self.contact=self.user_contact()

        self.sda=self.user_sda()
        if self.data.has_key('virtual'):
            # virtual number
            self.sid=None
        else:
            # real user
            self.sid=self.data['LocationDialingCode']

    def user_sda(self):
        """
            user has sda ?
        """
        #if self.username.startswith('+'):
        if self.username[0] in "+0123456789":
            sda=True
        else:
            sda=False
        return sda

    def user_contact(self):
        """

        """
        try:
            contact=self.data['contact']
        except KeyError:
            #contact=self.data['username']
            contact=self.username
        if contact.startswith('+'):
                contact=contact[1:]
        try:
            contact,domain=contact.split('@')
        except ValueError:
            # ValueError: need more than 1 value to unpack
            pass
        return contact

    def to_user(self,format_="universal"):
        """
            return the number to reach this user in a given format

            2525 , 122515 , +33146502515 , 033146502515

        """
        contact=self.contact
        assert format_ in self.number_formats , "format '%s' is not on %s" %(format_,str(self.number_formats))
        #['international','escape_international','escape_national','sid_ext','ext','contact']
        if format_ == "contact" or format_=="c":
            return contact
        # is it a virtual number
        elif self.data.has_key('virtual'):
            # abbreviated number (the username)
            return self.data['username']
        # has it sda or did
        elif self.sda == False:
            # no sda user
            if format_=='contact' or format_=="c":
                return contact
            # test if short number is set for the use
            try:
                number=self.data['number']
            except KeyError:
                raise ValueError("no SDA for user %s" % contact)
            # sda with short number set
            if format_=="ext" or format_=="e":
                return number
            elif format_=="sid_ext" or format_=="s":
                return str(self.sid) + number
            else:
                raise ValueError("no SDA for user %s" % contact)
        # number format and sda OK
        phone_number = FrenchPhoneNumber.parse_from_sip(self.username)
        if format_=='universal' or format_=="u":
            #return self.data['username']
            #return "+" + contact
            return phone_number.to_universal()

        elif format_=='international' or format_=='i':
            #return contact
            return phone_number.to_international(self.escape_prefix)

        elif format_=="escape_international" or format_=='xi':
            #return self.escape + "00" + contact
            #return self.escape + phone_number.to_international(self.escape_prefix_explicit)
            return phone_number.to_international(self.escape_prefix_explicit)

        elif format_=="escape_national" or format_=="xn":
            #return self.escape + "0" + contact[-9:]
            #return self.escape + phone_number.to_national(self.escape_prefix_explicit)
            return phone_number.to_national(self.escape_prefix_explicit)

        elif format_ == 'national' or format_=='n':
            #return contact[-9:]
            return phone_number.to_national(self.escape_prefix)
        # elif format_== 'international':
        #     return contact
        elif format_.startswith("sid") or format_=="s":
            return str(self.sid) + contact[-4:]
        elif format_=="ext" or format_=='e':
            return contact[-4:]
        else:
            raise ValueError("number format not implemented: %s" % format_)

    def sip_address_to_user(self,format):
        """

        """
        number = self.to_user(format)
        return "sip:%s@%s" % (number,self.data['domain'])

    @property
    def user_display(self):
        """
            return user display name
        """
        return self.data['display']

    @property
    def user_allias(self):
        """
            return user allias name
        """
        return self.data['allias']





class Users():
    """

        extract user data from enterprise hierarchy

        need data from ptf
            main:
                # default
                domain
                password
            enterprises:
                sites,users
            unallocated:
                sites
    """
    def __init__(self,data):
        """
            data for users from ptf

        """
        #self.data=data
        self.setup(data)

    def setup(self,data):
        """

        """
        assert data['main']
        assert data['enterprises']

        self.data=data

        self._enterprises={}
        self._sites={}
        self._users={}

        default_domain=self.data['main']['domain']
        default_password=self.data['main']['password']

        # for each enterprise (enterprise_1 ...)
        for enterprise_code,enterprise_data in self.data['enterprises'].iteritems():
            # set enterprise index  eg e['2361EVQ1']= enterprise_1
            enterprise_name=enterprise_data['name']
            self._enterprises[enterprise_name]=enterprise_data
            # for each site
            for site_code,site_data in enterprise_data['sites'].iteritems():
                site_name=site_data['name']
                # set site index
                self._sites[site_name]=site_data
                # for each user
                for user_allias,user_data in site_data['users'].iteritems():
                    # set user default entry eg u[Bob] = {
                    self._users[user_allias]= { 'domain': default_domain ,'password': default_password }
                    # set user data
                    self._users[user_allias].update(user_data)
                    # complete info site enterprise
                    self._users[user_allias]['enterprise']=enterprise_data
                    self._users[user_allias]['site']=site_data
                    self._users[user_allias]['allias']=user_allias
        return

    def iter_user_allias(self):
        """
        """
        for user_allias in self._users.keys():
            yield  user_allias

    # def user(self,allias):
    #     """
    #         return a User instance
    #     """
    #     return User(self._users[allias])
    #
    # def destination(self,name):
    #     """
    #         return a destination ( wrong_Sid_E1S1, wrong_Extentsion ... )
    #     """
    #     return self.data['destinations'][name]





class FeatureAccessCode():
    """
        data from base
    """
    def __init__(self,data):
        """
            data for feature access code
        """
        self.data=data

    def get(self,name):
        """
            get the fac code eg *21  from fac name

            @name:str , eg +CFA  (Call Forward Alwais
        """
        return self.data[name][0]

    def get_comment(self,name):
        """
            get the fac comment eg  from fac name

            @name:str , eg +CFA  (Call Forward Allwais
        """
        return self.data[name][1]

    def iter(self):
        for name,value in self.data.iteritems():
            yield name,value




class TerminalOptions(object):
    """
        handle the options of a pjsip terminal

    """
    levels= ["system","platform","device","account","session"]
    default_options= {
        "system": {
                    "--log-level": 5, "--app-log-level": 4, "--stdout-refresh": 2 , '--null-audio': '' },
        "platform": {
                    "--registrar": None ,          # 193.253.72.124:5060;lr
                    "--realm": "*",                # sip.imsnsn.fr
                    "--password": None,            # nsnims2008
        },
        "device": {
                    "--max-calls": 4 },

        "account": {
                "--id": None,                      # sip:+33960813200@sip.imsnsn.fr
                "--username": None,                # +33960813200@sip.imsnsn.fr
        },
        "session": {
                "--local-port": 6000,
                "--rtp-port":   20000,
        }

    }


    def __init__(self,default=None):
        """

        :param default:
        :return:
        """
        self.options={}
        self._with_audio= False
        self._codecs= {}


        self.options.update(self.default_options)
        #self.options= deepcopy(self.default_options)

        if default:
            self.options.update(deepcopy(default))

    def set_system(self,options):
        self.options['system'].update(options)

    def set_device(self,options):
        self.options['device'].update(options)

    def set_platform(self,options):
        self.options['platform'].update(options)

    def set_account(self,options):
        self.options['account'].update(options)

    def set_session(self,options):
        self.options['session'].update(options)

    def set_with_audio(self):
        self._with_audio= True

    def set_null_audio(self):
        self._with_audio= False


    def add_options(self,options,level="session"):
        """

            add a list of options to a specific level
        :param options: list of options tuples eg ["--add
        :param level:
        :return:
        """
        raise NotImplementedError


    def get_options(self):
        """
            get a dictionary of computed options ( hierachic resolution)
        :return:
        """
        opt= {}
        opt.update(self.options['system'])
        opt.update(self.options['platform'])
        opt.update(self.options['device'])
        opt.update(self.options['account'])
        opt.update(self.options['session'])

        # detect none value
        nones= [ key for key,value in opt.iteritems() if value==None]
        if nones:
            # some mandatory elements are None
            raise ValueError("None is not a correct value for options: %s" % ",".join(nones))

        # adjust with_audio
        if self._with_audio:
            # remove null audio
            try:
                del opt['--null-audio']
            except:
                pass
        else:
            # add null audio
            opt['--null-audio']= ''


        return opt

    def gen(self):
        """
        generate options , return a list of options compatible with pjsip
        :return:
        """
        opt= self.get_options()

        # handle codecs 1/2
        #  remove --add-codec and --dis-codec from options and generate codec line parameters
        codec_parameters= self.handle_codecs(opt)


       #  if self._codecs:
       #      # at least one codec has been set: remove default codec and add speciied codecs
       #      try:
       #          del opt['--add-codec']
       #      except:
       #          pass

        opts= [ "%s %s" % (key,value) for key,value in opt.iteritems() ]

        # handle codec 2/2
        opts.extend(codec_parameters)
        #opts.extend(self.get_codecs())

        return opts

    def clone(self):
        """

        :return:
        """
        new= TerminalOptions(self.options)
        new._with_audio= self._with_audio
        new._codecs=self._codecs

        return new

    def clear_codecs(self):
        """

        :return:
        """
        self._codecs= {}

    def add_codec(self,*names):
        """

        :return:
        """
        if isinstance(names,basestring):
            names= [ names, ]
        for name in names:
            self._codecs[name]=None

    def clear_codec(self,*names):
        """

        :param name:
        :return:
        """
        if isinstance(names,basestring):
            names= [ names, ]
        for name in names:
            try:
                del self._codecs[name]
            except:
                pass

    def get_codecs(self):
        """

        :return:
        """
        lc= [ "--add-codec %s" % codec for codec,dummy in self._codecs.iteritems() ]
        return lc

    def handle_codecs(self,options):
        """

            take a dict of options and return --add-codec and dis-codec parameter list

        :param options:
        :return:
        """
        codecs= []
        try:
            add_codec= options.pop('--add-codec')
            add_codec= self.to_list(add_codec)
        except KeyError:
            add_codec=[]
        try:
            dis_codec= options.pop('--dis-codec')
            dis_codec= self.to_list(dis_codec)
        except KeyError:
            dis_codec=[]


        # remove any add-codec present in dis-codec
        for d in dis_codec:
            try:
                i= add_codec.index(d)
                del add_codec[i]
            except ValueError:
                #not in list : pass
                pass


        codecs= [ '--add-codec %s' % codec for codec in  add_codec]
        dis= [ '--dis-codec %s' % codec for codec in  dis_codec]
        codecs.extend(dis)
        return codecs

    def to_list(self,line):
        """
            transform a multi option line into a list

            eg  "PCMA,PCMU" ->      [ 'PCMA' , 'PCMU']
                'PCMA'      ->      ['PCMA']
                ['PCMA','PCMU'] ->  [ 'PCMA' , 'PCMU']

        :param line: list or comma separated string
        :return: list
        """
        if isinstance(line,list):
            # already a list return it
            pass
        elif isinstance(line,basestring):
            # it is a string, remove spaces
            line=line.replace(' ','')
            if ',' in line:
                #comma separated
                line= line.split(',')
            else:
                # single option
                line= [line]
        elif line is None:
            line=[]
        else:
            raise ValueError("wrong format for list: %s" % line)
        return line

class DeviceSection(object):
    """
        a section of platform.json describing devices

        eg

      devices:
        default: {}
        ims:
          '--no-tcp': ''
          '--auto-update-nat': 0
          '--disable-via-rewrite': ''
          '--disable-rport': ''
          '--disable-stun': ''
          '--add-codec': 'PCMA,PCMU'

    """
    def __init__(self,data):
        """

        :param data: dict of device options
        :return:
        """
        assert isinstance(data,dict)

        self.data= data
        if not data.has_key('default'):
            data['default']= {}


    def get_device(self,name='default'):
        """

        :param name: str, the name of the device profile: default, ims .
        :return:
        """
        try:
            return self.data[name]
        except KeyError:
            raise KeyError('unknown device profile in common/devices section: %s' % name)




class SypConfiguration(object):
    """
            a platform.(json|yaml) handler
    """
    default_terminal_system_options= { "--log-level": 5, "--app-log-level": 4, "--stdout-refresh":2 }

    def __init__(self,data):
        """
        @data dict:  the data of a syp configuration ( with platform )
        :return:
        """
        self.data= data


    @classmethod
    def from_json(cls,fname):
        """

        :param fname: the json configuration filename (platform.json)
        :return:
        """
        with open( fname,"rb") as fh:
            platforms_data = json.loads(fh.read())
            conf= cls(platforms_data)
            return conf


    @classmethod
    def from_yaml(cls,fname):
        """

        :param fname: the json configuration filename (platform.yaml)
        :return:
        """
        if yaml_module:
            stream=file(fname)
            data = load(stream, Loader=Loader)
            return cls(data)
        else:
            raise ImportError("no yaml module")


    @classmethod
    def from_file(cls,filename):
        """

        :param filename:
        :return:
        """
        if filename.endswith(".yaml"):
            return cls.from_yaml(filename)
        else:
            return cls.from_json(filename)


    def get_ptf(self,ptf_name):
        """
            return a SypPlatform object for the given platform name

        :param ptf_name:
        :return:
        """
        ptf_data= self.data[ptf_name]
        ptf= SypPlatform(ptf_data,self)
        return ptf



class SypPlatform(object):
    """
        a representation of a platform

        initialized with data extracted from platform.json


        services:

            user_profile(alice) =  dict like platform.ini





    """

    def __init__(self,data,master=None):
        """
            data extracted from a platform.json
        """
        self.content = data
        self.master= master
        if master:
            assert isinstance(master,SypConfiguration)
            self.master=master


        self._user_profiles = None

        self.set_main_parameters()
        self.set_base_parameters()


        # setup device_section
        device_section= {}
        common= master.data.get('common')
        if common:
            device_section= master.data['common'].get('devices',{})
        self.device_section = DeviceSection(device_section)

        # setup a base terminal with system and platform info
        self.set_terminal_profile()


        return


    def set_main_parameters(self):
        """

        """
        main=self.content['main']
        self.ptf_context=deepcopy(main)

        self.ptf_name=self.ptf_context['platform_name']

        # does the platform need an escape char to place call outside
        # btic need 0 before national and international number ( escape="0")
        # btelu doesn not need esacape to place call outside ( escape ="")
        self.ptf_context['escape_prefix'] = main.get('escape_prefix',"")
        self.ptf_context['escape_prefix_explicit'] = main.get('escape_prefix_explicit',"0")

        address,port=main['proxy'].split(':')
        self.ptf_context['peer_adrress']=address
        self.ptf_context['peer_port']=port

        #self.ptf_context['local_port']=main['local_port_policy']
        #local_port=main['local_port_policy']
        #local_port= 0

        # gen_scenario interface
        proxy={'peer_address':address, 'peer_port':port}
        #s=g.get_scenario(simple,expand=True)

        # scenario generator
        #self.scenario_on_the_fly=gen_scenario.Platform("BTIC",proxy,local_port=local_port)

        # user related handler
        self._users=Users(self.content)

        return

    def set_base_parameters(self):
        """

        """
        # copy Feature Acces Code from gen_script_base
        #fac=deepcopy(self.base.content['FeatureAccessCode'])
        fac = self.content['FeatureAccessCode']
        # update if necessary
        # if self.content.has_key('FeatureAccessCode'):
        #     # overule general fac with ptf fac
        #     fac.update(self.content['FeatureAccessCode'])
        # make FAC handler
        self._feature_access_code=FeatureAccessCode(fac)


    def set_terminal_profile(self):
        """
            elaborate a base Terminal configuration (pjsip options) from platform.sjon

            from
                  "domain": "bluebox",
                  "password": "1234",
                  "profile": "default",
                  "proxy": "192.168.1.50:5060",

                      "username": "1000"
            towards:

                --id sip:+33146511101@192.168.1.50
                --registrar sip:192.168.1.50:5060
                --realm 192.168.1.50
                --username +33146511101
                --password 1234
                --use-timer 1



        :return:
        """
        self.terminal= TerminalOptions()

        # device option
        device_profile= self.content['main'].get('device_profile',None)
        if device_profile:
            # a profile is specified
            device=self.device_section.get_device(device_profile)
        else:
            # use default device
            device= {}
        self.terminal.set_device(device)

        # platform options
        platform = {
            "--realm":          self.content['main']['domain'],
            "--password":       self.content['main']['password'],
        }

        #
        # interpret main section of platform.json to get platform config
        #
        registrar= self.content['main'].get('registrar',None)
        proxy= self.content['main'].get('proxy',None)
        realm= self.content['main'].get('domain',"*")

        platform['--realm']= realm

        if registrar:
            # explicit registrar
            platform['--registrar']= registrar
            if proxy.startswith("sip:"):
                platform['--proxy']= proxy
            else:
                platform['--proxy'] = "sip:%s" % proxy
        else:
            # non explicit registrar: use proxy as registar
            if proxy.startswith('sip:'):
                platform['--registrar'] = proxy
            else:
                platform['--registrar'] = "sip:%s" % proxy

        self.terminal.set_platform(platform)


    def user_profile(self,role):
        """
            return a dictionary of users ( like platform.ini)

                [Alice]
                LocationDialingCode: 11
                display: Alice
                domain: bluebox
                password: cocoon
                username: +33146502510

        """
        if not self._user_profiles:

            # build user profiles
            users=self._users._users

            self._user_profiles = {}
            # generate dialplan

            # for each user allias
            for allias , data in users.iteritems():
                # set default for user
                #user=dict(domain=self.content['main']['domain'],password=self.content['main']['password'],
                #          profile=self.content['main']['profile'],allias= allias)

                user=dict(domain=self.content['main']['domain'],password=self.content['main']['password'],
                          allias= allias)


                #user['username']=data['username']
                user['username'] = "%s@%s" %(  data['username'],data['domain'] )
                user['display'] = allias
                # handle inpu id public identity (id) , if not speficied default to username (private or inpi)
                user['id']= "%s@%s" %(  data.get('id',data['username']),data['domain'] )

                # set custom values
                for field in ['domain','password','number','target','virtual','profile','display']:
                    if data.has_key(field):
                        user[field]=data[field]
                # set sid
                if data['site'].has_key('LocationDialingCode'):
                    user['LocationDialingCode']=data['site']['LocationDialingCode']
                else:
                    assert data['site']['name']=='virtual' , "no LocationDialingCode, site name should be 'virtual' and is not"

                # add registrar and proxy
                user['registrar'] = "sip:" + self.registrar()
                user['proxy'] = "sip:" + self.proxy()

                # add user to conf
                self._user_profiles[allias]=user
                continue

        return self._user_profiles[role]


    def user(self,role_name):
        """
            return a user profile
        """
        user_data = self.user_profile(role_name)
        return User(user_data,self.ptf_context['escape_prefix'],self.ptf_context['escape_prefix_explicit'])


    def destination(self,name):
        """
            return a destination number  eg 122515 for wrong_Sid_E1S2
        """
        return self.content['destinations'][name]


    def domain(self):
       return self.content['main']['domain']

    def registrar(self):
        """
        """
        if self.content['main'].has_key('registrar'):
            registrar = self.content['main']['registrar']
        else:
            registrar = self.content['main']['domain']
        return registrar

    def proxy(self):
        """
        """
        return self.content['main']['proxy']

    def profile(self):
        """

        """
        return self.content['main']['profile']

    def media_dir(self):
        """
            return the media_dir field of the pltaform s main section
        :return:
        """
        md= self.content['main'].get('media_dir','/usr/share/sounds/syprunner')
        return md


    def fac(self,name):
        """
            get feature access code , name "+CFA , _CFA ...
        """
        return self._feature_access_code.get(name)

    def iter_fac(self):
        return self._feature_access_code.iter()




    def sip_address_for(self,number):
        """
            number can be a fac code or destination in domain
        """
        return "sip:%s@%s" % (number,self.domain())


    def iter_user(self):
        """
            return a User instance for each user of the configuration
        """
        for user_allias in self._users.iter_user_allias():
            yield self.user(user_allias)


    def resolve_address(self,user=None,fac=None,format=None,destination=None,to_sip=True):

        result = ""
        if not fac :
            # a simple user or destination
            if not user:
                # a destination
                result= self.destination(destination)
            else:
                if not format:
                    format='contact'
                # a simple user destination with format
                return self.user(user).sip_address_to_user(format)
        else:
            # fac is not None
            if not user:
                # a fac alone or destination+ fac
                if not destination:
                    # a fac alone
                    result = self.fac(fac)
                else:
                    # a fac + destination
                    result = self.fac(fac) + self.destination(destination)
            else:
                # a user + fac
                result = self.fac(fac) + self.user(user).to_user(format)

        if not result:
            raise ValueError("cannot resolve destination")
        if to_sip:
            return self.sip_address_for(result)
        else:
            return result


    def sd8(self,role,sd):
        """
            direct keys SD8  1 to 8

            return  target

            @sd: string : 1 2 ... 8
        """
        target_user = None
        try:
            user = self._users._users[role]
        except KeyError:
            raise KeyError('no user %s' % role)

        try:
            sd_entry = user['sd8'][sd]
        except KeyError:
            raise KeyError('user %s has no sd %s' % (role,sd))
        if ',' in sd_entry:
            target_user , format = sd_entry.split(',')
        else:
            target_user=sd_entry
            format = None


        return target_user



    def update_terminal_with_account(self,terminal,allias,configuration=None):
        """

        :param terminal:
        :param allias:
        :param configuration:
        :return:
        """

        # get agent info from platform.json
        agent= self._users._users[allias]

        # handle private identity
        agent['id']= agent.get('id',agent['username'])

        # set account with platform.json info
        account= {
            '--id':           'sip:%s@%s' %( agent['id'],terminal.options['platform']['--realm'])  ,
            #'--username':     agent['username'],
            '--username':   '%s@%s' % ( agent['username'],terminal.options['platform']['--realm'] )

        }
        # special bluebox domain
        if agent['domain']=='bluebox':
            account['--username']= agent['username']


        if agent.has_key('password'):
            account['--password']= agent['password']

        # update account with custom configuration
        if configuration:
            assert isinstance(configuration,dict)
            account.update(configuration)

        # commit
        terminal.set_account(account)

        return terminal


    def get_terminal(self,allias,device=None,account=None):
        """
            obtain a terminal with system and platform info , optionaly with device and account info

        :param allias: str, agent name like Alice , Bob
        :param device: dict , config of the device eg:  { '--no-tcp':''}
        :param account: a dict of custom configuration  eg { '--max-calls': 2 }
        :return:
        """
        # get a default terminal
        terminal= self.terminal.clone()
        if device:
            terminal.set_device(device)
        if allias:
            if account is None:
                account= {}
            self.update_terminal_with_account(terminal,allias,account)
        return terminal


    def update_terminal_with_session(self,terminals,local_port=6000,rtp_port=20000):
        """

        """
        #local_port= 6000
        #rtp_port  = 20000

        # update terminals with session info ( local_port, rtp_port )
        for indice, terminal in enumerate(terminals):

            # set session
            session= {
                '--local-port': local_port +1 ,
                '--rtp-port':   rtp_port,
            }
            terminal.set_session(session)

            # get terminal options
            terminal_options= terminal.get_options()


            # update session parameters
            local_port= local_port +1
            rtp_port= rtp_port + (int(terminal_options['--max-calls']) * 2)

        return




def make_platform_ini_data(ptf_data):
    """

        ptf_data = like ptf.yaml

    """
    ptf=SypPlatform(ptf_data)


    conf_ini = dict (
        type = dict(category='platform'),
        platform= dict(
            registrar=ptf.content['main']['domain'],
            proxy= "sip:%s" % ptf.content['main']['proxy'],
            domain=ptf.content['main']['domain'],
            password=ptf.content['main']['password'],
            profile=ptf.content['main']['profile'],

            #root = conf.content['runner_dir'],
            base = ptf.content['platform_instance'],

        ),
        numbers= ptf.content['destinations'],
        FeatureAccessCode=ptf.content['FeatureAccessCode']
    )
    # compute media and logs dir
    conf_ini['platform']['media'] = os.path.join(conf_ini['platform']['root'],"var/scripts",conf_ini['platform']['base'],"media")
    conf_ini['platform']['logs'] = os.path.join(conf_ini['platform']['root'],"var/scripts",conf_ini['platform']['base'],"logs")

    # generate dialplan
    users=ptf._users._users
    # for each user allias
    for allias , data in users.iteritems():
        # set default for user
        user=dict(domain=ptf.content['main']['domain'],password=ptf.content['main']['password'])
        #user['username']=data['username']
        user['username']="%s@%s" %(  data['username'],data['domain'] )
        user['display']=allias

        # set custom values
        for field in ['domain','password','number','target','virtual']:
            if data.has_key(field):
                user[field]=data[field]
        # set sid
        if data['site'].has_key('LocationDialingCode'):
            user['LocationDialingCode']=data['site']['LocationDialingCode']
        else:
            assert data['site']['name']=='virtual'
        # add user to cof
        conf_ini[allias]=user
        continue

    return conf_ini


def gen_platform_ini(ptf_data,gen_dir):
    """
        generate platform.ini or dialplan for sypterm



    """
    conf_ini = make_platform_ini_data(ptf_data)

    # build conf
    lines=[]
    # gen type and platform sections
    for section in ['type','platform']:
        lines.append('[%s]' % section)
        for k in sorted(conf_ini[section].keys()):
            lines.append("%s: %s" % (k,conf_ini[section][k]))
        del conf_ini[section]
        lines.append('')
    # gen users
    for section in sorted(conf_ini.keys()):
        if section not in ['numbers','FeatureAccessCode']:
            lines.append('[%s]' % section)
            for k in sorted(conf_ini[section].keys()):
                lines.append("%s: %s" % (k,conf_ini[section][k]))
            del conf_ini[section]
        lines.append('')
    # gen numbers
    for section in ['numbers']:
        lines.append('[%s]' % section)
        for k in sorted(conf_ini[section].keys()):
            lines.append("%s: %s" % (k,conf_ini[section][k]))
        del conf_ini[section]
    lines.append('')
    # gen Feature Access code
    for section in ['FeatureAccessCode']:
        lines.append('[%s]' % section)
        for k in sorted(conf_ini[section].keys()):
            lines.append("# %s" % (conf_ini[section][k][1]))
            lines.append("%s: %s" % (k,conf_ini[section][k][0]))
        del conf_ini[section]


    # generate scripts
    out_path=gen_dir

    conf_name=os.path.join(out_path,'platform.ini')
    with open(conf_name,"w") as fh:
        fh.write("\n".join(lines))


    return conf_name




if __name__ == "__main__":


    fname= "../player/platform.json"
    fname_yaml= "../player/platform.yaml"
    platform_version= "demo_qualif"


    def test_overview():

        print "overview"

        # # assume it is a filename , load it
        # with open( fname,"rb") as fh:
        #     platforms = json.loads(fh.read())
        #     conf = platforms[platform_version]
        #     ptf = SypPlatform(conf)


        cnf= SypConfiguration.from_file(fname)
        ptf= cnf.get_ptf(platform_version)


        alice= ptf.user('Alice')

    def test_yaml():
        cnf= SypConfiguration.from_file(fname_yaml)
        ptf= cnf.get_ptf(platform_version)

        alice= ptf.user('Alice')
        print alice.to_user()
        print alice.sip_address_to_user(format='universal')

        alfred= ptf.user('Alfred')
        print alfred.to_user()
        print alfred.sip_address_to_user(format='universal')
        print alfred.sip_address_to_user(format='national')
        print alfred.sip_address_to_user(format='ext')
        print alfred.sip_address_to_user(format='sid_ext')
        print alfred.sip_address_to_user(format='contact')


        return

    def test_TerminalOptions():
        """


        :return:
        """
        o= TerminalOptions()

        o.set_device({
            "--no-tcp":"",
            "--auto-update-nat": 0,
            "--disable-stun": "",
            "--disable-via-rewrite":"",
            "--disable-rport":"",
            "--norefersub":"",
            "--reg-timeout": 300,
            "--add-codec": "PCMA",
            "--max-calls": 4,

        })
        o.set_platform({

            "--password": "nsnims2008",
            "--registrar": "sip:sip.imsnsn.fr",
            "--realm": "sip.imsnsn.fr",
            "--proxy":"sip:193.253.72.124:5060;lr",
            "--nameserver": "213.56.88.194",
        })

        o.set_account({
            "--username": "+33960813200@sip.imsnsn.fr",
            "--id":"sip:+33960813200@sip.imsnsn.fr",
            #"--password": "specific_password",
        })

        o.set_session({
            "--local-port": 6002,
            "--rtp-port": 20004,
            "--max-calls": "2"
        })

        o.set_with_audio()

        opts= o.gen()

        print  " ".join(opts)


        t2= o.clone()
        t2.set_account({
            "--username": "+33960813299@sip.imsnsn.fr",
            "--id":"sip:+33960813299@sip.imsnsn.fr",
            "--password": "specific_password",
        })

        t2.set_null_audio()

        print " ".join(t2.gen())

        print

        print " ".join(o.gen())


        return


    def test_platform_interface():
        """

        :return:
        """
        cnf= SypConfiguration.from_file(fname_yaml)
        ptf= cnf.get_ptf(platform_version)


        # create terminals ( with accounts , without session)
        # terminals= ptf.get_terminals(['Alice','Bob','Charlie'])
        # terminals= ptf.get_terminals(['Alice','Bob','Charlie'],(2,4,2))
        # terminals= ptf.get_terminals(['Alice','Bob','Charlie'],[2,4,2])

        #terminals= ptf.make_terminals(('Alice','Bob','Charlie'))


        # device configuration
        btelu_device= {
            '--no-tcp':'',
            '--auto-update-nat': 0,
            '--disable-via-rewrite':'',
            '--disable-rport':'',
            '--disable-stun':'',
            '--add-codec': 'PCMA',
        }

        users= ('Alice','Bob','Charlie')
        user_config= (
            {'--max-calls':2},
            {'--add-codec':'PCMU'},
            {},
        )


        terminals= []

        # create terminals with system,platform,device and account info
        for indice,allias in enumerate(users):

            # clone a basic terminal with system and platform info
            #terminal= ptf.get_terminal(allias,device=btelu_device, account= user_config[indice])
            terminal= ptf.get_terminal(allias, account= user_config[indice])
            terminals.append(terminal)

        # update all terminals with session info
        ptf.update_terminal_with_session(terminals,local_port=6000,rtp_port=20000)

        # compute terminal options
        terminal_configurations = [ " ".join(term.gen()) for term in terminals ]

        # display terminal option
        for t in terminal_configurations:
            print t


        return


    def test_DeviceSection():
        """

        :return:
        """
        cnf= SypConfiguration.from_file(fname_yaml)
        ptf= cnf.get_ptf(platform_version)


        default= ptf.device_section.get_device('default')
        ims= ptf.device_section.get_device('ims')
        try:
            bad= ptf.device_section.get_device('bad')
        except KeyError:
            pass


    def test_codecs():
        """

        :return:
        """
        cnf= SypConfiguration.from_file(fname_yaml)
        ptf= cnf.get_ptf(platform_version)

        allias= 'Alice'
        terminal= ptf.get_terminal(allias)

        terminal.set_session({'--add-codec': 'PCMA'})

        print " ".join(terminal.gen())


        print "add codecs"
        terminal.add_codec('PCMU')
        terminal.add_codec('speex')

        terminal.add_codec('other','another')

        terminal.clear_codec('another','dummy')

        print " ".join(terminal.get_codecs())


        print " ".join(terminal.gen())

        return


    # test begins here
    #test_overview()
    test_yaml()

    #test_TerminalOptions()
    test_platform_interface()
    #test_DeviceSection()
    #test_codecs()



    print


