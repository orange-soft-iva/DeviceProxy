__author__ = 'cocoon'
"""

    tools to scan pjsua logs

"""
import re
from call_quality import CallQuality


class SipScanner():
    """
        scan lines with (feed(line) ), to retrieve begin and end of received sip messages
        inside a sip message perform some task
            like memoize from: header
            store lines for message

    """


    # SIP MESSAGE BEGINS LIKE: pjsua_core.c  .RX 1140 bytes Request msg INVITE/cseq=50631033 (rdata0x7fe20d004628) from UDP 172.16.229.139:5060:
    begin_rx_msg = re.compile(r"pjsua_core.c  .RX .* Request msg INVITE/" )

    # sip message ends with --end msg--
    end_msg = re.compile(r'--end msg--')

    # from header is like From: "2510" <sip:2510@172.16.229.139>;tag=D8SjZ1QU6tQ1c
    from_header = re.compile(r'From: ')


    # detect start of last response received  eg , SIP/2.0 200 OK   200/response
    # pjsua_core.c  .RX 559 bytes Response msg 200/REGISTER/cseq=9189 (rdata0x7fc24c80ec28) from UDP 172.16.229.144:5060:
    begin_rx_response = re.compile(r'pjsua_core.c  .RX .*? Response msg (.*?)/(.*?)/cseq=(\d+) .*')

    #
    #begin_status = re.compile(r"dump call quality status" )
    #end_status   = re.compile(r"--end status--" )
    begin_status = re.compile(r"pjsua_app_comm !" )
    end_status   = re.compile(r"RTT msec" )

    def __init__(self):
        """

        """
        # state None INVITE,BYE ...
        self.state=None      #  states: None , .RX
        self.kind=None
        self.last = dict(
            INVITE = {},
            BYE = {},
            NOTIFY = {},
        )

    def feed(self,line):
        """
            analyse line
        """


        if self.state == None:
            # search "pjsua_core.c  .RX 1140 bytes Request msg INVITE" : the begin of a received sip message
            m = self.begin_rx_msg.search(line)
            if m:
                # we detect start of a received sip request message
                self.state=".RX"
                self.kind= "INVITE"
                self.last[self.kind] = { 'lines': [] , 'from': "" , 'ready': False}
                return self.state
            # search start of a sip response
            m = self.begin_rx_response.search(line)
            if m:
                # we detect start of a received sip response message
                code = m.group(1)
                op = m.group(2)
                cseq = m.group(3)
                self.state=".RX"
                self.kind= "%s/%s" % (code,op)   # can be '200/INVITE'  '200/REGISTER'
                self.last[self.kind] = { 'lines': [] , 'ready': False , 'cseq': cseq }
                return self.state
            else:
                # not a start sip
                # search for begin status
                m = self.begin_status.search(line)
                if m:
                    # we detect a status start
                    self.state = "STATUS"
                    self.kind  = "media_status"
                    self.last[self.kind] = {'lines':[], 'ready': False}
                    return self.state
                else:
                    # neither starts a sip message or a status message : skip
                    return self.state


        elif self.state == ".RX":
            # inside a received sip message ( either request or response)
            if self.end_msg.search(line):
                # we receive end of sip message
                self.last[self.kind]['ready'] = True
                self.state=None
                self.kind=None
            else:
                # inside receiving msg search From: "2510" <sip:2510@172.16.229.139>;tag=tK90Q85UeFXye
                # add the line
                self.last[self.kind]['lines'].append(line)
                # detect from line
                m = self.from_header.search(line)
                if m :
                    # from header , save it to last['INVITE']['from']
                    self.last[self.kind]['from'] =line

        elif self.state == "STATUS":
            # inside a dump call status
            if self.end_status.search(line):
                # include the last line
                self.last[self.kind]['lines'].append(line)
                # we receive end of dump status
                self.last[self.kind]['ready'] = True
                self.state=None
                self.kind=None
                return "media status done"
            else:
                # inside receiving msg search From: "2510" <sip:2510@172.16.229.139>;tag=tK90Q85UeFXye
                # add the line
                self.last[self.kind]['lines'].append(line)
        return '.'


    def serialize_status(self,lines):
        """
            extract info from dump call quality status

            sample:
                userB: [CONFIRMED] To: "2510" <sip:2510@172.16.229.144>;tag=Br4BB6DmejcNF
                userB:   Call time: 00h:00m:33s, 1st res in 106 ms, conn in 157ms
                userB:   #0 audio PCMA @8kHz, sendrecv, peer=-
                userB:      SRTP status: Not active Crypto-suite: (null)
                userB:      RX pt=8, last update:00h:00m:01.387s ago
                userB:         total 0pkt 0B (0B +IP hdr) @avg=0bps/0bps
                userB:         pkt loss=0 (0.0%), discrd=0 (0.0%), dup=0 (0.0%), reord=0 (0.0%)
                userB:               (msec)    min     avg     max     last    dev
                userB:         loss period:   0.000   0.000   0.000   0.000   0.000
                userB:         jitter     :   0.000   0.000   0.000   0.000   0.000
                userB:      TX pt=8, ptime=20, last update:never
                userB:         total 595pkt 94.4KB (118.2KB +IP hdr) @avg=22.2Kbps/27.8Kbps
                userB:         pkt loss=0 (0.0%), dup=0 (0.0%), reorder=0 (0.0%)
                userB:               (msec)    min     avg     max     last    dev
                userB:         loss period:   0.000   0.000   0.000   0.000   0.000
                userB:         jitter     :   0.000   0.000   0.000   0.000   0.000
                userB:      RTT msec      :   0.000   0.000   0.000   0.000   0.000
                userB:


            return data

                call "2510"
                    status: CONFIRMED
                    call_time: 00h:00m:33s
                    media_slot #0:
                        format: PCMA
                        frequency: 8kHz
                        mode: sendrecv

                        RX:
                            total_pkt: 0
                            total_bytes: 0
                            loss: 0
                            loss_percent: 0

                        TX:
                            total_pkt: 0
                            total_bytes: 0
                            loss: 0
                            loss_percent: 0


        """
        # create and return a call quality object
        data = [ line.strip()  for line in lines   ]
        metrics =  CallQuality(data)
        return metrics


    def get_state(self,kind = 'media_status'):
        """
            kind: either media_status , INVITE ...

        """
        return self.last[kind]

    def get_last_response(self,code,op):
        """
            return the last response receive for this code and  operation

            code:str  200 , 202 ..
            op: str  , INVITE , REGISTER ...
        """
        key = "%s/%s" % (str(code),op.upper())
        sip_response = self.last[key]
        #assert sip_response['ready'] == True
        return sip_response

    def get_last_request(self,operation):
        """
            return the last request received for this operation
        """
        sip_request = self.last[operation.upper()]
        return sip_request
