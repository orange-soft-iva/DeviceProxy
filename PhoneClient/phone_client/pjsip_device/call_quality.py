__author__ = 'cocoon'


import re


class CallQuality():
     """
        extract info from dump call quality status display

        sample:
            userB: [CONFIRMED] To: "2510" <sip:2510@172.16.229.144>;tag=Br4BB6DmejcNF
            userB:   Call time: 00h:00m:33s, 1st res in 106 ms, conn in 157ms
            userB:   #0 audio PCMA @8kHz, sendrecv, peer=-
            userB:      SRTP status: Not active Crypto-suite: (null)
            userB:      RX pt=8, last update:00h:00m:01.387s ago
            userB:         total 0pkt 0B (0B +IP hdr) @avg=0bps/0bps
            userB:         pkt loss=0 (0.0%), discrd=0 (0.0%), dup=0 (0.0%), reord=0 (0.0%)
            userB:               (msec)    min     avg     max     last    dev
            userB:         loss period:   0.000   0.000   0.000   0.000   0.000
            userB:         jitter     :   0.000   0.000   0.000   0.000   0.000
            userB:      TX pt=8, ptime=20, last update:never
            userB:         total 595pkt 94.4KB (118.2KB +IP hdr) @avg=22.2Kbps/27.8Kbps
            userB:         pkt loss=0 (0.0%), dup=0 (0.0%), reorder=0 (0.0%)
            userB:               (msec)    min     avg     max     last    dev
            userB:         loss period:   0.000   0.000   0.000   0.000   0.000
            userB:         jitter     :   0.000   0.000   0.000   0.000   0.000
            userB:      RTT msec      :   0.000   0.000   0.000   0.000   0.000
            userB:


        return data

            call "2510"
                status: CONFIRMED
                call_time: 00h:00m:33s
                media_slot #0:
                    format: PCMA
                    frequency: 8kHz
                    mode: sendrecv

                    RX:
                        total_pkt: 0
                        total_bytes: 0
                        loss: 0
                        loss_percent: 0

                    TX:
                        total_pkt: 0
                        total_bytes: 0
                        loss: 0
                        loss_percent: 0


     """
     #[CONFIRMED] To: "2510" <sip:2510@172.16.229.144>;tag=Br4BB6DmejcNF
     call_header_pattern = re.compile(r'\s*\[(.*)\] To\: (.*?);tag\=(.*)')

     #   Call time: 00h:00m:33s, 1st res in 106 ms, conn in 157ms
     call_time_pattern = re.compile(r'\s*Call time: (.*?)\,(.*)')

     # slot info
     #  #0 audio PCMA @8kHz, sendrecv, peer=-
     slot_pattern = re.compile(r'\s*#(\d+) audio (.*?) @(.*?)(k?Hz), (.*?), (.*)')

     #      RX pt=8, last update:00h:00m:01.387s ago
     rx_pattern = re.compile(r'\s*RX pt=(.*?), .*')

     #      TX pt=8, ptime=20, last update:never
     tx_pattern = re.compile(r'\s*TX pt=(.*?), .*')

     # total 595pkt 94.4KB (118.2KB +IP hdr) @avg=22.2Kbps/27.8Kbps
     # total 2.3Kpkt 374.5KB (468.2KB +IP hdr) @avg=63.9Kbps/79.9Kbps
     total_pattern= re.compile(r'\s*total (\d+[\.\d]*K?)pkt (.*?)(K?B) (.*)')

     # packet line pattern
     #   pkt loss=0 (0.0%), dup=0 (0.0%), reorder=0 (0.0%)
     packet_pattern= re.compile(r'\s*pkt loss=(\d+) \((.*?)%\), (.*)')



     def __init__(self,lines):
        """

        """
        self.metrics = self.parse(lines)


     def parse(self,lines):
        """

        """
        state = None
        sub_state=None
        status = []
        calls = {}
        call_specifier = None
        slot_specifier = None
        slot = None
        for line in lines:
            if not line:
                # end
                break

            # detect call specifier
            #[CONFIRMED] To: "2510" <sip:2510@172.16.229.144>;tag=Br4BB6DmejcNF
            m = self.call_header_pattern.search(line)
            if m:
                # found a call header
                call_status = m.group(1)
                call_specifier = m.group(2)
                calls[call_specifier]= { 'status': call_status, 'name': call_specifier, 'slots': {}}
                state = 'CALL'
                continue
            else:
                # not a next call go on with last one
                pass

            if state == 'CALL':
                # extract call info
                current_call={}
                m = self.call_time_pattern.search(line)
                if m:
                    call_time= m.group(1)
                    other =m.group(2)
                    calls[call_specifier]['call_time'] = call_time
                    state = "SLOT"
                else:
                    state = "ERROR"

            elif state == 'SLOT':

                # detect new slot
                m = self.slot_pattern.search(line)
                if m:
                    slot_specifier = m.group(1)
                    calls[call_specifier]['slots'][slot_specifier]={}
                    slot = calls[call_specifier]['slots'][slot_specifier]
                    slot['audio'] = m.group(2)
                    slot['frequency'] = int(m.group(3))
                    slot['frequency_unit'] = m.group(4)
                    slot['mode'] = m.group(5)
                    other =m.group(6)
                    sub_state = None
                    continue


                # search for SRTP or TX or RX
                if sub_state != "TX":
                    m = self.tx_pattern.search(line)
                    if m :
                        sub_state = "TX"
                        pt = m.group(1)
                        slot[sub_state]={}
                        slot[sub_state]['pt'] = pt
                        continue

                if sub_state != "RX":
                    m = self.rx_pattern.search(line)
                    if m :
                        sub_state = "RX"
                        pt = m.group(1)
                        slot[sub_state]={}
                        slot[sub_state]['pt'] = pt
                        continue
                else:
                    # SRTP or RTT : do not handle
                    #sub_state = None
                    #continue
                    pass



                #TODO: refactor to group RX TX sub_state
                # handle sub_state
                if sub_state == 'TX':
                    # search for total
                    m = self.total_pattern.search(line)
                    if m :
                        total_packets= m.group(1)
                        total_size = m.group(2)
                        total_size_unit= m.group(3)
                        other = m.group(4)

                         # treat total
                        if total_packets.endswith('K'):
                            packets = total_packets.rstrip('K')
                            packets = float(packets) * 1000
                        else:
                            packets = int(total_packets)


                        slot[sub_state]['packets'] = int(packets)
                        slot[sub_state]['size'] = float(total_size)
                        slot[sub_state]['size_unit'] = total_size_unit
                        continue
                    # search for pkt line
                    m = self.packet_pattern.search(line)
                    if m :
                        loss_packets= m.group(1)
                        loss_percent = m.group(2)
                        other = m.group(3)
                        slot[sub_state]['loss_packets'] = int(loss_packets)
                        slot[sub_state]['loss_percent'] = float(loss_percent)
                        continue
                    # ignore other TX lines
                    continue

                elif sub_state == 'RX':
                    # search for total
                    m = self.total_pattern.search(line)
                    if m :
                        total_packets= m.group(1)
                        total_size = m.group(2)
                        total_size_unit= m.group(3)
                        other = m.group(4)

                        # treat total
                        if total_packets.endswith('K'):
                            packets = total_packets.rstrip('K')
                            packets = float(packets) * 1000
                        else:
                            packets = int(total_packets)

                        slot[sub_state]['packets'] = int(packets)
                        slot[sub_state]['size'] = float(total_size)
                        slot[sub_state]['size_unit'] = total_size_unit
                        continue
                    # search for pkt line
                    m = self.packet_pattern.search(line)
                    if m :
                        loss_packets= m.group(1)
                        loss_percent = m.group(2)
                        other = m.group(3)
                        slot[sub_state]['loss_packets'] = int(loss_packets)
                        slot[sub_state]['loss_percent'] = float(loss_percent)
                        continue
                    # ignore other RX lines
                    continue
                else:
                    # dont handle other sub_state
                    continue


            elif state == "ERROR":

                raise RuntimeError('Incorrect dump quality line: %s' % line)

            else:

                raise RuntimeError('Incorrect dump quality state: %s' % state)

        return calls



     def packets_statistics(self, channel= 'TX',  slot = '0' , call = 0):
         """
            return packets stats for a given call /slot / channel
         """
         #TODO get the right call instead of random call
         call_tag = self.metrics.keys()[0]
         call = self.metrics[call_tag]
         slot = call['slots'][slot]
         return slot[channel]


     def loss_packets(self,channel='TX' ,slot='0', call = 0):
         """
            return loss packets and loss rate for a given call /channel
         """
         stats = self.packets_statistics(channel,slot,call)
         return(stats['loss_packets'], stats['loss_percent'])



if __name__=="__main__":



    sample ="""\
[CONFIRMED] To: "2510" <sip:2510@172.16.229.144>;tag=Br4BB6DmejcNF
  Call time: 00h:00m:33s, 1st res in 106 ms, conn in 157ms
  #0 audio PCMA @8kHz, sendrecv, peer=-
     SRTP status: Not active Crypto-suite: (null)
     RX pt=8, last update:00h:00m:01.387s ago
        total 0pkt 0B (0B +IP hdr) @avg=0bps/0bps
        pkt loss=0 (0.0%), discrd=0 (0.0%), dup=0 (0.0%), reord=0 (0.0%)
              (msec)    min     avg     max     last    dev
        loss period:   0.000   0.000   0.000   0.000   0.000
        jitter     :   0.000   0.000   0.000   0.000   0.000
     TX pt=8, ptime=20, last update:never
        total 595pkt 94.4KB (118.2KB +IP hdr) @avg=22.2Kbps/27.8Kbps
        pkt loss=0 (0.0%), dup=0 (0.0%), reorder=0 (0.0%)
              (msec)    min     avg     max     last    dev
        loss period:   0.000   0.000   0.000   0.000   0.000
        jitter     :   0.000   0.000   0.000   0.000   0.000
     RTT msec      :   0.000   0.000   0.000   0.000   0.000

"""

    sample2 ="""\
[CONFIRMED] To: "2510" <sip:2510@172.16.229.144>;tag=Br4BB6DmejcNF
  Call time: 00h:00m:33s, 1st res in 106 ms, conn in 157ms
  #0 audio PCMA @8kHz, sendrecv, peer=-
     SRTP status: Not active Crypto-suite: (null)
     RX pt=8, last update:00h:00m:01.387s ago
        total 0pkt 0B (0B +IP hdr) @avg=0bps/0bps
        pkt loss=0 (0.0%), discrd=0 (0.0%), dup=0 (0.0%), reord=0 (0.0%)
              (msec)    min     avg     max     last    dev
        loss period:   0.000   0.000   0.000   0.000   0.000
        jitter     :   0.000   0.000   0.000   0.000   0.000
     TX pt=8, ptime=20, last update:never
        total 595pkt 94.4KB (118.2KB +IP hdr) @avg=22.2Kbps/27.8Kbps
        pkt loss=0 (0.0%), dup=0 (0.0%), reorder=0 (0.0%)
              (msec)    min     avg     max     last    dev
        loss period:   0.000   0.000   0.000   0.000   0.000
        jitter     :   0.000   0.000   0.000   0.000   0.000
     RTT msec      :   0.000   0.000   0.000   0.000   0.000
  #1 audio PCMA @8kHz, sendrecv, peer=-
     SRTP status: Not active Crypto-suite: (null)
     RX pt=8, last update:00h:00m:01.387s ago
        total 0pkt 0B (0B +IP hdr) @avg=0bps/0bps
        pkt loss=0 (0.0%), discrd=0 (0.0%), dup=0 (0.0%), reord=0 (0.0%)
              (msec)    min     avg     max     last    dev
        loss period:   0.000   0.000   0.000   0.000   0.000
        jitter     :   0.000   0.000   0.000   0.000   0.000
     TX pt=8, ptime=20, last update:never
        total 595pkt 94.4KB (118.2KB +IP hdr) @avg=22.2Kbps/27.8Kbps
        pkt loss=0 (0.0%), dup=0 (0.0%), reorder=0 (0.0%)
              (msec)    min     avg     max     last    dev
        loss period:   0.000   0.000   0.000   0.000   0.000
        jitter     :   0.000   0.000   0.000   0.000   0.000
     RTT msec      :   0.000   0.000   0.000   0.000   0.000
[CONFIRMED] To: "2511" <sip:2510@172.16.229.144>;tag=Br4BB6DmejcNF
  Call time: 00h:00m:33s, 1st res in 106 ms, conn in 157ms
  #0 audio PCMA @8kHz, sendrecv, peer=-
     SRTP status: Not active Crypto-suite: (null)
     RX pt=8, last update:00h:00m:01.387s ago
        total 0pkt 0B (0B +IP hdr) @avg=0bps/0bps
        pkt loss=0 (0.0%), discrd=0 (0.0%), dup=0 (0.0%), reord=0 (0.0%)
              (msec)    min     avg     max     last    dev
        loss period:   0.000   0.000   0.000   0.000   0.000
        jitter     :   0.000   0.000   0.000   0.000   0.000
     TX pt=8, ptime=20, last update:never
        total 595pkt 94.4KB (118.2KB +IP hdr) @avg=22.2Kbps/27.8Kbps
        pkt loss=0 (0.0%), dup=0 (0.0%), reorder=0 (0.0%)
              (msec)    min     avg     max     last    dev
        loss period:   0.000   0.000   0.000   0.000   0.000
        jitter     :   0.000   0.000   0.000   0.000   0.000
     RTT msec      :   0.000   0.000   0.000   0.000   0.000
  #1 audio PCMA @8kHz, sendrecv, peer=-
     SRTP status: Not active Crypto-suite: (null)
     RX pt=8, last update:00h:00m:01.387s ago
        total 0pkt 0B (0B +IP hdr) @avg=0bps/0bps
        pkt loss=0 (0.0%), discrd=0 (0.0%), dup=0 (0.0%), reord=0 (0.0%)
              (msec)    min     avg     max     last    dev
        loss period:   0.000   0.000   0.000   0.000   0.000
        jitter     :   0.000   0.000   0.000   0.000   0.000
     TX pt=8, ptime=20, last update:never
        total 2.3Kpkt 374.5KB (468.2KB +IP hdr) @avg=63.9Kbps/79.9Kbps
        pkt loss=5 (0.5%), dup=0 (0.0%), reorder=0 (0.0%)
              (msec)    min     avg     max     last    dev
        loss period:   0.000   0.000   0.000   0.000   0.000
        jitter     :   0.000   0.000   0.000   0.000   0.000
     RTT msec      :   0.000   0.000   0.000   0.000   0.000



"""


    def test():

        lines = sample.split('\n')
        dq = CallQuality(lines)
        print dq.metrics






        lines = sample2.split('\n')
        dq = CallQuality(lines)
        print dq.metrics


        assert dq.metrics['"2511" <sip:2510@172.16.229.144>']['slots']['1']['TX']['packets'] == 2300
        assert dq.metrics['"2511" <sip:2510@172.16.229.144>']['slots']['1']['TX']['loss_packets'] == 5
        assert dq.metrics['"2511" <sip:2510@172.16.229.144>']['slots']['1']['TX']['loss_percent'] == 0.5
        assert dq.metrics['"2511" <sip:2510@172.16.229.144>']['slots']['1']['TX']['pt'] == '8'
        assert dq.metrics['"2511" <sip:2510@172.16.229.144>']['slots']['1']['TX']['size'] == 374.5
        assert dq.metrics['"2511" <sip:2510@172.16.229.144>']['slots']['1']['TX']['size_unit'] == 'KB'
        count,rate = dq.loss_packets()

        return




    #### begins here

    test()