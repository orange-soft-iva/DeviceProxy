
import os
import time
import re
from dvclient.agent import Agent



from logscanner import SipScanner


# where to find media files on target
SYPRUNNER_MEDIA_DIR = "/tests/media"


# demo mode
SYPRUNNER_BLUEBOX_DOMAIN ='bluebox'
# place the ip of the demo freeswitch in ENV var SYPRUNNER_BLUEBOX
SYPRUNNER_BLUEBOX_IP = os.environ.get('SYPRUNNER_BLUEBOX_IP',SYPRUNNER_BLUEBOX_DOMAIN)





class TestError(Exception):
    pass
    #desc = ""
    #
    def __init__(self, desc):
        self.desc = desc


class TimeoutError(TestError):
        pass



class expect_sypterm():
    """

    """
    # The command prompt
    PROMPT=  ">>>"
    # When pjsua has been destroyed
    DESTROYED= "PJSUA destroyed"
    # Assertion failure
    ASSERT= "Assertion failed"


    # Stdout refresh text
    #15:48:31.255    pjsua_app.c !STDOUT_REFRESH
    STDOUT_REFRESH= "STDOUT_REFRESH"
    STDOUT_REFRESH_PATTERN= re.compile("^[> ]*([^\s]+)\s+pjsua_app.c .*?%s" % "STDOUT_REFRESH")




    # sip:+33146511101@192.168.1.50: registration success, status=200 (OK)
    REGISTERED= ".* registration success, status=(\d+) (.*)"
    #REGISTERED= ".*?Registration complete.*?status=\s*(.*?)\s+(.*)"


    # .Incoming Request msg INVITE/cseq=75122085
    #EVENT_INCOMING_CALL = ">*> Incoming call"
    #EVENT_INCOMING_CALL = '.*Transaction .* state changed to Proceeding.*'
    EVENT_INCOMING_CALL= "Incoming Request msg INVITE/cseq="

    # Press a to answer or h to reject call
    ANSWER_PROMPT= "Press a to answer or h to reject call"


    # 16:49:16.252 dlg0x7fb20c81b  ......Transaction tsx0x7fb20c81b8a8 state changed to Proceeding

    ##########################
    # CALL STATES
    #

    # Call state is CALLING
    #STATE_CALLING = "state.*CALLING"

    CALLING= '.*state changed to Calling'

    COMPLETED= '.*state changed to Completed'

    #Call 0 is DISCONNECTED [reason=200 (Normal call clearing)]
    #HANGUP= '.*State changed from .* to Completed'
    HANGUP= 'Call .* is DISCONNECTED .*'


    CANCEL_RECEIVED= "CANCEL sip:"


    UNREGISTERED= '.*unregistration success'

    # Call state is EARLY
    STATE_EARLY = "state.*EARLY"
    # Call state is CONFIRMED
    STATE_CONFIRMED = "state.*CONFIRMED"
    # Call state is DISCONNECTED
    STATE_DISCONNECTED = "Call .* DISCONNECTED"

    # Media call is put on-hold
    MEDIA_HOLD = "Call [0-9]+ media [0-9]+ .*, status is .* hold"
    # Media call is active
    MEDIA_ACTIVE = "Call [0-9]+ media [0-9]+ .*, status is Active"
    #MEDIA_ACTIVE = "Media for call [0-9]+ is active"
    # RX_DTMF
    RX_DTMF = "Incoming DTMF on call [0-9]+: "



    # pjsua_app.c  .......Call 0: transfer status=200 (OK) [final]
    #REFER_INCOMING_RESPONSE= 'Incoming Response msg 202/REFER/cseq='
    REFER_INCOMING_RESPONSE='Call (\d+): transfer status=(\d+).*'

    # 13:58:25.227 pjsua_app_lega !Current dialog: "+33146511103" <sip:+33146511103@192.168.1.50>
    PREVIOUS_CALL_SELECTED= 'pjsua_app_lega !Current dialog: (.*)'
    NEXT_CALL_SELECTED=     'pjsua_app_lega !Current dialog: (.*)'


    ##########################
    # MEDIA
    #

    # Connecting/disconnecting ports
    MEDIA_CONN_PORT_SUCCESS =    "Port \d+ \(.+\) transmitting to port"
    MEDIA_DISCONN_PORT_SUCCESS = "Port \d+ \(.+\) stop transmitting to port"

    # Filename to play / record
    MEDIA_PLAY_FILE= "--play-file\s+(\S+)"
    MEDIA_REC_FILE=  "--rec-file\s+(\S+)"

    #CALL_QUALITY_DONE= '.*call quality done'


    # pjsua_aud.c !Call 0 dialing DTMF 123
    # DTMF digits enqueued for transmission
    #DTMF_SENT= 'pjsua_aud.c .*Call (\d+) dialing DTMF (.*)'
    DTMF_SENT= 'DTMF digits enqueued for transmission'

    # pjsua_app.c  .Incoming DTMF on call 0: 1
    CHECK_DTMF= "pjsua_app.c  .*Incoming DTMF on call (\d+): (.)"


    # pjsua_call.c !Putting call 0 on hold
    # Call 0 media 0 [type=audio], status is Local hold
    #PUT_CALL_ON_HOLD= 'Incoming Response msg 200/INVITE'
    #PUT_CALL_ON_HOLD= 'Call (\d+) media (\d+) \[type=audio\], status is Local hold'
    PUT_CALL_ON_HOLD= 'Call (\d+) media (\d+) \[type=audio\], status is (.*)'


    # pjsua_app.c  .....Call 0 media 0 [type=audio], status is Active
    UNHOLD_CALL= 'Call (\d+) media (\d+) \[type=audio\], status is Active'


    #START_PLAYING_FILE= 'start playing file'
    START_PLAYING_FILE= '.*conference.c .* transmitting to port 0 .*'
    # 16:41:31.591   conference.c  .Port 4 (/Users/cocoon/Documents/hgclones/syprunner/player/media/sample.wav) transmitting to port 0 (Master/sound)

    #STOP_PLAYING_FILE=  'stop playing file'
    STOP_PLAYING_FILE=  '.*conference.c .* stop transmitting to port 0 .*'
    # 16:30:37.789   conference.c  .Port 4 (/Users/cocoon/Documents/hgclones/syprunner/player/media/sample.wav) stop transmitting to port 0 (Master/sound)

    #START_RECORDING_FILE= 'recorder started'
    START_RECORDING_FILE= '.* pjsua_aud.c !Conf connect: \d+ --> 1.*'
    # 16:44:17.597    pjsua_aud.c !Conf connect: 3 --> 1


    #STOP_RECORDING_FILE=  'recorder stopped'
    STOP_RECORDING_FILE=  '.*pjsua_aud.c !Conf connect: \d+ --> 1.*'
    # 18:49:22.948    pjsua_aud.c !Conf connect: 3 --> 1

    @classmethod
    def CALL_STATE(cls,state):
        assert state.upper() in ['CALLING','CONFIRMED','DISCONNECTED']
        if state.upper() == 'CONFIRMED':
            # Call 0 state changed to CONFIRMED
            #return '.*state changed to Terminated'
            return '.*state changed to CONFIRMED'

        elif state.upper() == 'DISCONNECTED':
            #return '.*state changed to Completed'
            return 'pjsua_app.c .*CAll \d+ is DISCONNECTED .*'

        elif state.upper() == 'CALLING':
            return '.*state changed to Calling'
        else:
            # never reach this: the old way to check_call
            return (">*> CALL %s" % state.upper())


    @classmethod
    def WAIT_INCOMING_RESPONSE(cls,code,operation,cseq=''):
        """

        :return:
        """
        #pattern = "Incoming Response msg %s/%s/cseq=%s" % (str(code),operation.upper(),cseq)
        pattern = "RX .* Response msg %s/%s/cseq=%s" % (str(code),operation.upper(),cseq)
        return pattern

    @classmethod
    def WAIT_INCOMING_REQUEST(cls,operation,cseq=''):
        """

        :return:
        """
        #pattern = "Incoming Response msg %s/%s/cseq=%s" % (str(code),operation.upper(),cseq)
        pattern = "RX .* Request msg %s/cseq=%s" % (operation.upper(),cseq)
        return pattern



    @classmethod
    def STDOUT_REFRESH_TIME(cls,line):
        """
            return None if not a refresh line
                return the text timestamp other wise
        """
        timestamp= None
        match= expect_sypterm.STDOUT_REFRESH_PATTERN.match(line)
        if match:
            timestamp= match.group(1)

        return timestamp

hint= expect_sypterm



# Error handling
def handle_error(errmsg, pilot, close_processes=False):
    """
        @pilot : instance od SypBot
    """
    pilot.log("====== Caught error: " + errmsg + " ======")
    if (close_processes):
        pilot.log("close agents")
        time.sleep(1)
        for p in pilot.agents.values():
            # Protect against 'Broken pipe' exception
            try:
                p.send("q")
                p.send("q")
            except:
                pass
            is_err = False
            try:
                ret= p.expect(hint.DESTROYED, False)
                if not ret:
                    is_err = True
            except:
                is_err = True
            if is_err:
                pilot.log("no Destroyed confirmation received")
                #if sys.hexversion >= 0x02060000:
                #    p.proc.terminate()
                #else:
                #    p.wait()
            else:
                #p.wait()
                pilot.log("pjsip stack Destroyed")

    pilot.log( "<*> Test completed with error: " + errmsg )
    return 1




class PjsipAgent(Agent):
    """


    """
    version='pjsip_device'



    def setup(self,create=False):
        """


        :param create:
        :return:
        """
        super(PjsipAgent, self).setup(create=create)

        self.t0 = time.time()
        self.echo= True
        self.trace_enabled = True

        self.ra = re.compile(hint.ASSERT, re.I)

        # create a sip scanner
        self.sip=SipScanner()


        self.have_reg = True

    def remote_dummy(self):
        """


        :return:
        """
        print "pjsip device remote dummy"


    #
    #  base methods
    #

    def trace(self, s):
        if self.trace_enabled:
            now = time.time()
            fmt = "================== " + s + " ==================" + " [at t=%(time)03d]"
            self.log(fmt % {'time': int(now - self.t0)})

    def log(self,msg):
        print "%s: %s" % (self.identifier,msg)
        #self.pilot.out("%s: %s" % (self.name,msg))

    def sleep(self,seconds=1):
        """

        :param seconds:
        :return:
        """
        time.sleep(seconds)


    def send(self, cmd):
        self.trace("send " + cmd)
        self.write_stdin(message=cmd + "\n")

        #self.proc.stdin.writelines(cmd + "\n")
        #self.proc.stdin.flush()



    def expect(self, pattern, raise_on_error=True, title="",ignore_timeout=False,refresh_count=6):
        """
            expect a special pattern in terminal output flow


            read an output line of terminal output
                if corresponding to expect: return the line


                if corresponding to a trap assertion
                    raise TestError

                if corresponding to a REFRESH marker : count it
                    when count exceeds a limit
                        raise TimeoutError ( or return none if raise_on_error is false )


        """
        self.trace("expect " + pattern)
        start_time = time.strftime("%X") + ".000"

        synchronized = False
        refresh_count=int(refresh_count)

        # compute search pattern with ignore case
        r = re.compile(pattern, re.I)
        refresh_cnt = 0


        guard_counter= 5

        while True:
            # read line
            #line = self.proc.stdout.readline()
            line= self.read_stdout()


            if line is None:
                if guard_counter > 0:
                    self.log("read_stdout return None!")
                    self.sleep(1)
                    guard_counter -= 1
                    continue
                else:
                    # timeout
                    line= ""

            # catch broken pipe
            if line == "":
                raise TestError(self.name + ": Premature EOF")

            # Print the line if echo is ON
            if self.echo:
                self.log(line.rstrip())


            # Trap assertion error
            if self.ra.search(line) != None:
                if raise_on_error:
                    raise TestError(self.name + ": " + line)
                else:
                    return None

            # handle refresh for timeouts
            refresh_time= hint.STDOUT_REFRESH_TIME(line)
            if refresh_time:
                # got a refresh from terminal process
                if refresh_time > start_time:
                    if synchronized == False:
                        synchronized = True
                        self.log(" ... synchronized")
                    # Count stdout refresh text.
                    refresh_cnt = refresh_cnt + 1
                # check timeout
                if refresh_cnt >= refresh_count:
                    self.trace("Timed-out!")
                    if ignore_timeout == True:
                        self.trace("ignore timeout")
                        # this can result in a blocking situation : find a better way
                        continue
                    if raise_on_error:
                        raise TimeoutError(
                            self.name + " " + title + ": Timeout expecting pattern: \"" + pattern + "\"")
                    else:
                        return None        # timeout

            # feed sip analyser
            a=self.sip.feed(line)
            # check media status done
            if a == "media status done":
                # log scanner tell us about media status done
                if r.search(a):
                    # if it is what we search return the line
                    self.trace("found %s" % pattern)
                    return line

            # Search for expected text
            if r.search(line) != None:
                # we found the expected line , return it
                self.trace("found %s" % pattern)
                return line

    def wait(self):
        self.trace("wait")
        #self.proc.communicate()
        raise NotImplementedError


    #
    #  high level methods
    #

    def check_not_received(self,pattern,refresh_count=1):
        """
            check we dont receive a pattern in a given time

        """
        try:
            line = self.expect(pattern,refresh_count=refresh_count)
        except TimeoutError ,e:
            # a timeout occurs that is we were waiting for
            return None

        # any other case is an error
        raise TestError("check not received have failed")




    ### added functions
    def wait_register(self):
        """

        """
        status_code = None
        status_message = None
        line = None

        try:

            if self.have_reg:
                line= self.expect(hint.REGISTERED)
                match= re.match(hint.REGISTERED,line)

                if match:
                    status_code=match.group(1)
                    status_message=match.group(2)

            #
            self.send("echo 1")
            self.send("echo 1")
            self.expect("echo 1")


            if self.have_reg:
            # treat reponse send message
                if status_code:
                    if status_code=="200":
                        self.log("Registration OK")
                    else:
                        raise TestError('Registration failed : %s %s' % (status_code,status_message))
                else:
                    raise TestError('error scanning registration response: %s' % line)

        except TestError, e:
            return handle_error(e.desc, self.pilot)

        return 0

    def wait_timed_out(self):
        """
            wait for timed-out to occur to finish test with OK
        """
        self.expect("never catch this",raise_on_error=False)

    def watch_log(self,duration=2):
        """
            just listen and catch the log for a while

        """
        self.expect("never catch this", raise_on_error=False, title="watch_log",refresh_count=duration)


    def unregister(self):
        """
            unregister
        """
        self.send("ru")
        self.expect(hint.UNREGISTERED)

    def dump_call_quality_status(self,call_indice=None):
        """
            @call_indice: int : the indice of the call ( 0 , 1 .. or None for current call)

        """
        self.log("dump call quality status")
        self.send("dq")
        self.expect("media status done")


    def shutdown(self):
        """

        """
        # Protect against 'Broken pipe' exception
        try:
            self.send("q")
            self.send("q")
        except:
            pass
        is_err = False
        try:
            ret = self.expect(hint.DESTROYED, raise_on_error=False, ignore_timeout=False)
            if not ret:
                is_err = True
        except:
            is_err = True
        if is_err:
            self.log("No destroyed confirmation received")
        else:
            self.log("pjsip stack destroyed")

        return

    ##  higher level functions
    def call(self,destination):
        """
             other possible syntax: userA.call(userB,toB)

             @to : destination : the name of a user variable declared : toB , toC , to1 to2


        """
        self.log("call %s" % destination)

        # analyse parameter
        #sip_address= self.param[to.lower()]

        if not destination.startswith("sip:"):
            # it is a symbole: fetch the number
            sip_address= self.param[destination]
        else:
            # it is a full destination, leave it as is
            sip_address = destination

        ## filter uri for %23 compatiblity with EasyInjector
        if "%23" in sip_address:
            sip_address=sip_address.replace("%23","#")

        # intercept blubox domain for demo mode @bluebox
        if sip_address.endswith(SYPRUNNER_BLUEBOX_DOMAIN) :
            # eg replace sip:1000@bluebox with sip:1000@192.168.1.50
            sip_address = sip_address.replace(SYPRUNNER_BLUEBOX_DOMAIN,SYPRUNNER_BLUEBOX_IP)


        self.send("m")
        self.send(sip_address)

        self.expect(hint.CALLING)
        self.pilot.sleep(0.1)
        return

    def answer_call(self,code=200,wait_incoming=True):
        """


        """
        self.log("answer_call(code=%s)" % str(code))
        self.pilot.sleep(0.1)

        if wait_incoming :
            # wait for incoming call
            self.expect(hint.EVENT_INCOMING_CALL)

        self.send('a')
        self.send(str(code))
        if str(code) == "200":
            self.expect(hint.STATE_CONFIRMED)
            self.pilot.sleep(0.1)
        elif str(code) in ["180","486","487","480"]:
            # ringing , busy here
            self.pilot.sleep(1)

        else:
            raise NotImplementedError("answer_call with code: %s is not implemented" % str(code))

    def wait_incoming_call(self):
        """
            wait incoming call and return from line
        """
        #self.expect(self.pilot.const.EVENT_INCOMING_CALL)
        self.expect(hint.EVENT_INCOMING_CALL)
        from_line = self.sip.last['INVITE']['from']
        return from_line



    def hangup(self,call_indice=None):
        """


        """
        if call_indice is None:
            self.log("hangup the current call")
            self.send('h')
        else:
            self.log("hangup the call with indice: %s" %  str(call_indice))
            self.send('h%s' % call_indice )

        self.expect(hint.HANGUP)

    def wait_hangup(self):
        """


        """
        self.log("wait_hangup()")
        self.expect(hint.HANGUP)


    def check_call(self,state="CONFIRMED"):
        """

        """
        self.log("check_call(%s)" % state)
        self.expect(hint.CALL_STATE(state))

    def check_media(self,ua2):
        self.send("#")
        self.expect("#")
        self.send("1122")
        #TODO: dont check receive DTMF
        #ua2.expect(const.RX_DTMF + "1")
        #ua2.expect(const.RX_DTMF + "1")
        #ua2.expect(const.RX_DTMF + "2")
        #ua2.expect(const.RX_DTMF + "2")

    def send_dtmf(self,digits):
        """

        :param digits:
        :return:
        """
        digits=str(digits)
        self.log('send dtmf sequence: %s' % digits)
        self.send("#")
        self.send(str(digits))
        self.expect(hint.DTMF_SENT)

    def check_no_dtmf(self,refresh_count=1):
        """
            check we dont receive any dtmf sequence
        :param count:
        :return:
        """
        self.log("check no incoming dtmf")
        self.check_not_received(hint.CHECK_DTMF,refresh_count)
        self.log('no incoming dtmf received: ok')

    def check_dtmf(self,digits):
        """
            check we received a dtmf sequence
        :param digits: str , the digits to be received
        :return:
        """
        digits=str(digits)
        self.log("check receive dtmf sequence: %s" % digits)
        for digit in digits:
            self.log('check dtmf: %s' % digit)
            line= self.expect(hint.CHECK_DTMF)
            rx= re.compile(hint.CHECK_DTMF)
            match= rx.search(line)
            if match:
                received_digit= match.group(2)
                if digit == received_digit:
                    # ok we received the right digit
                    self.log("received dtmf is ok: %s" % digit)
                else:
                    msg= 'received bad dtmf: %s instead of %s'%(received_digit,digit)
                    self.log(msg)
                    raise ValueError(msg)
            else:
                msg= 'check dtmf problem on received line: %s' % line
                self.log(msg)
                raise ValueError(msg)
            continue
        self.log("received dtmf sequence ok: %s" % digits)

        return True

    def receive_cancel(self):
        self.expect(hint.CANCEL_RECEIVED)


    def wait_incoming_response(self,code,operation,cseq=''):
        """
            wait an incoming response  , 200/INVITE/cseq=

            @code: str , is either 200 , 407 , 202 ..
            @operation:str , either INVITE , REGISTER ,
            cseq:str , the sequence number: optional


        """
        #pattern = "Incoming Response msg %s/%s/cseq=%s" % (str(code),operation.upper(),cseq)
        pattern= hint.WAIT_INCOMING_RESPONSE(code,operation,cseq)
        self.expect(pattern)
        raw_response = self.sip.get_last_response(code,operation)
        return raw_response

    def wait_incoming_request(self,operation,cseq=''):
        """
            wait an incoming request  , INVITE/cseq= or CANCEL/cseq

            @operation:str , either INVITE , REGISTER ,
            cseq:str , the sequence number: optional


        """
        pattern= hint.WAIT_INCOMING_REQUEST(operation,cseq)
        self.expect(pattern)

        return True


    def transfer(self,destination):
        """
             blind transfer call to destination

             @to : destination : the name of a user variable declared : toB , toC , to1 to2


        """
        self.log("transfer call to destination %s" % destination)

        # analyse parameter
        #sip_address= self.param[to.lower()]

        if not destination.startswith("sip:"):
            # it is a symbole: fetch the number
            sip_address= self.param[destination]
        else:
            # it is a full destination, leave it as is
            sip_address = destination

        ## filter uri for %23 compatiblity with EasyInjector
        if "%23" in sip_address:
            sip_address=sip_address.replace("%23","#")

        self.send("x")
        self.send(sip_address)
        #self.expect('Incoming Response msg 202/REFER/cseq=')
        self.expect(hint.REFER_INCOMING_RESPONSE)

        # fix bug: dont wait for Bye
        #self.expect("Received Request msg BYE/cseq=")

        self.pilot.sleep(0.1)
        return


    def transfer_attended(self,call_indice=1):
        """

            attended transfer

        """
        self.log("transfer attended call to destination %s" % str(call_indice))


        self.send('X')
        self.send(call_indice)
        # if call_indice == 0:
        #     # transfer to call 0
        #     self.send("X")
        # else :
        #     cmd = "X" + str(call_indice)
        #     self.send(cmd)

        #self.expect("Incoming Response msg 202/REFER/cseq=")
        self.expect(hint.REFER_INCOMING_RESPONSE)
        self.pilot.sleep(0.1)

        return


    def hold(self,call_index=None):
        """
             put call on hold

             call_index= None:  hold the current call
                       = 0 : hold call index 0 ...

        """
        if call_index == None:
            self.log("put current call on hold " )
        else:
            self.log("put the call with index %s on  hold " % (str(call_index)))

        # wait to give a chance the call to be established
        time.sleep(0.5)

        # send the hold command to terminal
        if call_index == None:
            self.send("H")
        else:
            self.send("H%s" %  str(call_index))

        #self.expect("Incoming Response msg 200/INVITE")
        self.expect(hint.PUT_CALL_ON_HOLD)

        return

    def unhold(self,call_index=None):
        """
             cancel call on hold

             call_index= None:  unhold the current call
                       = 0 : unhold call index 0 ...


        """
        if call_index == None:
            self.log("unhold current call" )
        else:
            self.log("unhold the call with index %s" % (str(call_index)))

        # wait to give a chance the call to be established
        time.sleep(0.5)

        # send the hold command to terminal
        if call_index == None:
            self.send("v")
        else:
            self.send("v%s" % str(call_index))

        #self.expect("Incoming Response msg 200/INVITE")
        self.expect(hint.UNHOLD_CALL)

        return

    def start_player(self):
        """
            start playing file
        """
        #self.log("start player" )
        #self.send("p+")
        #self.expect(hint.START_PLAYING_FILE)
        self.log('set mode AutoPlay')
        self.send("AP+")

    def stop_player(self):
        """
            stop playing file
        """
        self.log('disable mode AutoPlay')
        self.send("AP-")

    def start_recorder(self):
        """
            start playing file
        """
        self.log('set mode AutoRecord')
        self.send("AR+")

    def stop_recorder(self):
        """
            stop playing file
        """
        self.log('disable mode AutoRecord')
        self.send("AR-")


    def select_previous_call(self):
        """
            select previous call
        """
        self.trace("select previous call" )
        self.send("[")
        self.expect(hint.PREVIOUS_CALL_SELECTED)

    def select_next_call(self):
        """
            select next call
        """
        self.log("select next call" )
        self.send("]")
        self.expect(hint.NEXT_CALL_SELECTED)


    def get_last_call_status(self):
        """
            get the last dumped quality status

            return a sypua.CallQuality instance
        """
        # get the status
        status_lines = self.sip.get_state('media_status')
        if status_lines:
            # make a CallQuality instance
            stats = self.sip.serialize_status(status_lines['lines'])
            return stats
        else:
            self.log("no metrics")
            raise ValueError("no stats are available")

    def check_call_quality(self,min_packets= 100 , max_loss_rate = 0.1, channel = 'RX', slot='0',call=None):
        """
            a simple call quality , we specify
                - a minimim number of packet to receive
                - a max loss rate
        """
        self.log("check_call_quality(%s/%s) on  channel %s" % (min_packets,max_loss_rate,channel))
        # get the status
        status_lines = self.sip.get_state('media_status')
        if status_lines:
            metrics = self.sip.serialize_status(status_lines['lines'])
            # get stats for call/slot/channel
            #count,rate = metrics.loss_packets('0',0)
            #self.log('metrics: loss packets: %s (%s)' % (count,rate))
            stats = metrics.packets_statistics(channel,slot,call)
            self.log('metrics: %s' % (str(stats)))
            return stats
        else:
            self.log("no metrics")
            return None

    def get_last_received_response(self,code,operation,cseq=None):
        """
            retrieve the last response received corresponding to code , operation and optional cseq

                eg 200/INVITE

        """
        raw_response = self.sip.get_last_response(code,operation)
        #TODO: check the cseq if asked
        return raw_response

    def get_last_received_request(self,operation,cseq=None):
        """
            retrieve the last request received corresponding to the operation and optional seq

            INVITE/

        """
        raw_request = self.sip.get_last_request(operation)
        #TODO: check the cseq if asked
        return raw_request



