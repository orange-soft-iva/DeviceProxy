
import os


from dvproxy.session_configuration import SessionConfiguration

from ptf_manager2 import SypConfiguration



# ENV var for pjsua transport configuration
SYPRUNNER_CONFIGURATION = dict (

    SYPRUNNER_BOUND_ADDR = None,
    SYPRUNNER_IP_ADDR = None,
    SYPRUNNER_OUTBOUND = None,
    SYPRUNNER_NAME_SERVER = None,

    # the base for the rtp port assignation
    #SYPRUNNER_RTP_PORT= 40000,
    SYPRUNNER_RTP_PORT= 20000,

    # the base for the local port assignation
    SYPRUNNER_LOCAL_PORT = 5060,

)

# where to find media files on target
SYPRUNNER_MEDIA_DIR = "/tests/media"


# demo mode
SYPRUNNER_BLUEBOX_DOMAIN ='bluebox'
# place the ip of the demo freeswitch in ENV var SYPRUNNER_BLUEBOX
SYPRUNNER_BLUEBOX_IP = os.environ.get('SYPRUNNER_BLUEBOX_IP',SYPRUNNER_BLUEBOX_DOMAIN)



def get_env_defaults():
    """
        return values of SYPRUNNER

    :return: dict ,
    """
    defaults= {}

    # set transport parameters from ENV and defaults
    for name in SYPRUNNER_CONFIGURATION.keys():
        env_var = os.environ.get(name,SYPRUNNER_CONFIGURATION[name])
        if name in ['SYPRUNNER_LOCAL_PORT',]:
            env_var = int(env_var)
        elif name in ['SYPRUNNER_RTP_PORT',]:
            # adapt start rtp_port  reserve 4 rtp_port for each terminal eg 40000 + 4
            if env_var :
                env_var = int(env_var)
            else :
                # ignore
                continue
        elif name in ['PJSUA_BOUND_ADDR',]:
            env_var = int(env_var)
        # translate ENV name into pjsua parameter name (truncate prefix + lower
        param_name = name[len('SYPRUNNER_'):].lower()
        defaults[param_name] = env_var

    return defaults




class PjsipSessionConfiguration(SessionConfiguration):
    """



    """

    def compute_pjterm_options(self,ptf,users_conf):
        """

        :return:
        """
        terminal_configurations=[]

        # common parameters for all agents ( codecs , play file
        params = {}

        start_rtp_port= 20000
        default_max_calls= 4



        #TODO: get device profile from platform.json
        device= 'default'
        devices= {
            'default': {

            },
            'ims': {
                '--no-tcp':'',
                '--auto-update-nat': 0,
                '--disable-via-rewrite':'',
                '--disable-rport':'',
                '--disable-stun':'',
                '--add-codec': 'PCMA',
                },
        }

        # get default environment var (SYPRUNNER_*) , local_port,rtp_port, outbound, bound_addr,ip_addr,name_server
        defaults= get_env_defaults()

        terminals= []

        # create terminals with system,platform,device and account info
        indices="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        for indice in xrange(0,len(users_conf)):
            # for each agent in the session
            allias= users_conf[indice]['allias']
            #agent_name = "user%s" % indices[indice]

            # clone a basic terminal with system and platform info
            terminal= ptf.get_terminal(allias,device=devices[device], account= {})

            # add customized rec-file  rec-user.wav, rec-user2.wav
            user_tag= 'user%s' % indices[indice]

            user_rec= 'rec-%s.wav' % user_tag
            terminal.set_session({'--rec-file': user_rec})

            # add customized play-file
            media_dir= ptf.media_dir()
            sample_name= user_tag + ".wav"
            play_file= os.path.join(media_dir,sample_name)

            try:
                # check file exist
                os.stat(play_file)
                terminal.set_session( {'--play-file': play_file})
            except OSError:
                print('WARNING cant open media file %s'% play_file)

            terminals.append(terminal)

        # update all terminals with session info
        local_port= defaults['local_port']
        rtp_port= defaults['rtp_port']
        ptf.update_terminal_with_session(terminals,local_port,rtp_port)

        # compute terminal options
        terminal_configurations = [ " ".join(term.gen()) for term in terminals ]

        # display terminal option
        for t in terminal_configurations:
            print t



        return  terminal_configurations


    def setup(self, parameters):
        """
        customisation for open a pjsip session

        analyse all configuration : compute command line for each pjsip terminal

        :return:
        """

        configuration= self._configuration

        # get the platform configuration
        platform= parameters['platform']
        platform_version= parameters['platform_version']


        if isinstance(platform,dict):
             sypconf= SypConfiguration(platform)
        else:
            sypconf= SypConfiguration.from_file(platform)

        ptf= sypconf.get_ptf(platform_version)

        # set the brute platform info
        self.conf= sypconf.data[platform_version]


        users_list= [ el[0] for el in configuration if el[1]== 'pjsip_device']
        users_conf=[ptf.user_profile(role) for role in users_list ]

        options= self.compute_pjterm_options(ptf,users_conf)


        # update pjsip config
        indice=0
        for agent_name in self.agent_list():
            agent= self.agent(agent_name)
            if agent['category'] == 'pjsip_device':
                self.set_agent_parameter(agent['identifier'],'command_line',options[indice])
                indice+=1

        return self.configuration()

