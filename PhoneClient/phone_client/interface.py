__author__ = 'cocoon'



class IPhoneInterface(object):
    """
        common interface for pjsip and android device

    """

    def phone_call(self,literal):
        """


        :param literal:
        :return:
        """
        raise NotImplementedError


    def phone_hangup(self):
        """

        :return:
        """
        raise NotImplementedError


    def phone_call_hold(self):
        """

        :return:
        """
        raise NotImplementedError


    def phone_call_reinvite(self):
        """

        :return:
        """
        raise NotImplementedError

    def phone_call_transfer(self):
        """

        :return:
        """
        raise NotImplementedError


    def phone_call_transfer_attended(self):
        """

        :return:
        """
        raise NotImplementedError

    def phone_send_dtmf(self):
        """

        :return:
        """
        raise NotImplementedError
