__author__ = 'cocoon'


import requests
import json


from dvclient.session import Session




class ApplicationSession(object):
    """

    """
    def __init__(self):
        """
        """
        self._store = {
            'accounts' : {},
            'current':  {}
        }



    def add_users(self,*accounts):
        """

        """
        for account in accounts:
            #key,param= account
            key= account[0]
            param= account[1:]
            self._store['accounts'][key]= param



    def current_session(self,data=None):
        """
            current session info
        """
        if not data:
            # read it
            return self._store['current']
        else:
            # set data
            self._store['current'] = data

    def close(self):
        """
        """
        self._store = {
            'accounts' : {},
            'current':  {}
        }





class Client(object):
    """

        a client hub

            parameters:
                url: site of remote server hub


    """
    session_type= 'client'

    allow_session_reopen= True
    disable_agent_locks= False

    def __init__(self,**parameters):
        """

        :return:
        """
        # a map of agent instances
        self._sessions={}
        self._agents = {}
        self.parameters=parameters

        self._web_session=None
        self._session=None

        self.setup()


    def set_web_session(self):
        """
        # set the requests session

        :param url:
        :return:
        """
        self._web_session=requests.Session()
        self._web_session.headers.update({
            'content-type': 'application/json; charset=utf-8',
            'accept': 'application/json',
            'Accept-Charset': 'utf-8'
            })

        return self._web_session


    def get_url(self, uri = None):
        """

        :param complement:
        :return:
        """
        if not uri:
            return self._url
        else:
            #return "/".join(self._url , uri )
            return "%s%s" % (self._url,uri)



    def setup(self,create=False):
        """



        :param create:
        :return:
        """

        # check parameters
        assert 'url' in self.parameters.keys()
        self._url= self.parameters['url']


        # set web session
        self.set_web_session()



    def open_session(self,configuration,**parameters):
        """

            open a session with configuration
                store accounts

         :param configuration:list, configuration of the session list of dict name,type,paramameters
         :param parameters: dict : parameters for the session type

         example of parameters: session_type, session_id ...
        :return:
        """
        if self._session:
            return self._session

        #
        # initialise a remote web session
        #

        # save session data
        self._session = ApplicationSession()
        self._session.add_users(* configuration)

        data= { 'configuration':configuration, 'parameters': parameters}

        # request to server
        data = json.dumps( data)
        url= self.get_url('/sessions')
        response = self._web_session.post(url,data=data)

        response_data = json.loads(response.content)
        #self._session.content = response_data
        # save the session data
        if response.status_code >= 200 and response.status_code < 300:
            self._session.current_session(response_data)
            #return True
            #return response_data
        else:
            raise RuntimeError('%s' % response.content)


        #
        # initialize local session
        #
        factory_name= self.session_type
        factory= Session.select_plugin('session',version=factory_name)
        # create session
        session=factory(self)

        session.open(configuration=configuration)


        # store session object
        self._sessions['default']=session

        return

    def close_session(self,session_id='default'):
        """

        :return:
        """

        # close remote web session
        session_name = self._session.current_session()['session']
        uri = '/sessions/%s' % session_name
        response = self._web_session.delete(self.get_url(uri))

        #self._session.close()
        #return response

        # close local session
        r= self.session(session_id).close()
        del self._sessions[session_id]

        # close local session

        return r

    def check_session(self,session_id='default'):
        """

        :param session_id:
        :return:
        """
        raise NotImplementedError
        # session= self.session(session_id)
        # return {'session.id':session_id}



    def clear_sessions(self):
        """

        """
        response = self._web_session.post(self.get_url('/sessions/-/clear_sessions'))

        response_data = json.loads(response.content)
        #self._session.content = response_data
        # save the session data
        if response.status_code >= 200 and response.status_code < 300:
            return True
        else:
            raise RuntimeError('%s' % response.content)



    # reserved
    def lock_agent(self,agent_id,**data):
        """
            return True if lock is obtained
            or false if agent is busy
        """
        #return self.locker.lock(agent_id,**data)
        return True


    def release_agent(self,agent_id):
        """

        """
        #return self.locker.release(agent_id)
        return True


    def __getattr__(self, function_name):
        """
            wrapper to agent methods

        :param function_name:
        :return:
        """
        # call a function
        def wrapper(agent_id,**kwargs):
            """

            """
            agent = self.agent(agent_id)
            # intercept timeout arguments if any
            if 'timeout' in kwargs:
                # convert timeout to int
                kwargs['timeout'] = int(kwargs['timeout'])
            try:
                function = getattr(agent,function_name)
            except AttributeError,e:
                raise e
            return function(**kwargs)
        return wrapper


    def session(self,session_id='default'):
        """

            :return the session by id

        :param session_id:
        :return:
        """
        try:
            return self._sessions[session_id]
        except KeyError:
            raise KeyError('no such session: %s' % session_id)

    def agent(self,agent_id,session_id='default'):
        """
            return an agent object by id in a specific section

        :param identifer:
        :return:
        """

        return self.session(session_id).agent(agent_id)

