__author__ = 'cocoon'


import json
from dvproxy.agent import Agent as BaseAgent


class Agent(BaseAgent):
    """

        a mix of dvproxy.agent , dvproxy.http_client


    """
    version= 'client'

    def check_parameters(self,parameters=None):
        """


        :param parameters:
        :return:
        """
        parameters = parameters or self.parameters
        for field in ['web_session','agent_name','url',]:
            assert field in parameters.keys()



    def setup(self,create=False):
        """

            assert web_session= in paramters

        :param create:
        :return:
        """
        self.check_parameters()

        # setup web session from parameters
        self._web_session = self.parameters.pop('web_session')
        self._agent_name  = self.parameters.pop('agent_name')
        self._url=          self.parameters.pop('url')



    #
    # httpclient part
    #
    def _handle_response(self,response):
        """
            handle the http response

        :param response: flask response object
        :return:  json_data or raise runtime error if http error encountered
        """
        # response analyse
        if response.status_code >= 200 and response.status_code < 300:
            # reponse ok
            try:
                json_data = response.json()
                result = json_data['result']
            except:
                result = None
        else:
            # other http result (404 , 500 )
            raise RuntimeError('failed: status:%s' % str(response.status_code))

        return result


    def __getattr__(self, function_name):
        """

        :param item:
        :return:
        """



        # call a function
        def wrapper(**kwargs):
            """

            """
            agent_name= self._agent_name
            # compose request
            return self._request(agent_name=agent_name,operation_name=function_name,**kwargs)

        return wrapper


    def _request(self,agent_name,operation_name,**kwargs):
        """
            launch a request

        :param agent_id:
        :param args:
        :param kwargs:
        :return:
        """
        # session=self._session.current_session()
        # try:
        #     agent_name = session[agent_id]
        # except KeyError:
        #     raise RuntimeError('no such agent [%s] for this session' % agent_id)

        url =  self._url + "/agents/%s/%s/" % (str(agent_name), operation_name)
        print "POST " + url + "  " + str(kwargs)
        response = self._web_session.post(url,data=json.dumps(kwargs))

        # response analyse
        if response.status_code >= 200 and response.status_code < 300:
            # reponse ok
            #response.encoding="utf-8"
            #response.encoding = 'ISO-8859-1'
            try:
                json_data = response.json()
                result = json_data['result']
            except:
                result = None
        else:
            # other http result (404 , 500 )
            json_data = response.json()
            # print json_data['message']
            # print json_data['exc_type']
            # print json_data['exc_value']

            text =[]
            text.append(json_data['message'])
            text.append(json_data['exc_type'])
            text.append(json_data['exc_value'])

            raise RuntimeError('failed: status:%s , message: %s' % (str(response.status_code),"  ".join(text)))

        return result






