

from dvproxy.session import Session as BaseSession

from dvclient.agent import Agent



class Session(BaseSession):
    """


        a client session

            creates http client for each kind of agent


    """
    version = 'client'


    def open(self,configuration=None):
        """
            init session in creating agents

        :param configurations: list , list of parameters for creating agents
        :return:

        eg    [

                { id1, factory_name, parameters },
                { id2, factory_name , parameters },
            ]

        """
        configuration = configuration or self.configuration

        rc= self.lock_agents(configuration)
        if rc:
            # lock succesfull
            # start an agent for each item of config
            for conf in configuration:
                # each configuration is a tuple ( agent_id , parameters )
                identifier, factory_name, parameters= conf


                # add web session parameters
                parameters['web_session'] = self.parent._web_session

                # add agent_name
                parameters['agent_name'] = self.parent._session.current_session()[identifier]

                # add url
                parameters['url']= self.parent._url


                # select agent plugin
                factory= Agent.select_plugin('agent',factory_name)

                # build agent
                agent = factory(identifier, **parameters)

                self._agents[identifier] = agent

            return self.setup(self.configuration)
        else:
            # lock agents has failed
            raise RuntimeError("Session canceled: one of the agent is locked")