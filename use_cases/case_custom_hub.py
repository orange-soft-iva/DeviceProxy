
import time

from dvproxy.hub import Hub
from dvproxy.agent import Agent
from dvproxy.session import Session


from dvproxy.adapter import ThreadedAdapter as Adapter
#from dvproxy.adapter import ProcessAdapter as Adapter

from dvproxy.adapter import ClientAdapter

from uiautomatorlibrary import Mobile
from uiautomatorlibrary.Mobile import ADB


class CustomSession(Session):
    """


    """
    version='phone'


    @classmethod
    def _adb(cls,cmd,serial_id=None,**kwargs):
        """


        :return:
        """
        d= ADB(serial_id)
        result= d.cmd(cmd)
        return result


    @classmethod
    def adb_devices(cls,**kwargs):
        """

        """
        return ADB().devices()


    # @classmethod
    # def clear_all(cls,**kwargs):
    #     """
    #         clear all session to release all locks
    #     """
    #     for session in cls.parent._iter_session():
    #         session.close()




class CustomAgent(Agent):
    """

    """
    version= 'base'
    def dummy(self,**kwargs):
        """
            a dummy operation returning the given arguments

        :param arg:
        :param kwargs:
        :return:
        """
        print "Agent %s is a (%s): dummy(%s)" % (self.identifier,self.version,str(kwargs))
        return kwargs

class SypTerm(CustomAgent):
    """

        pjterm-macosx-10.8-x86_64

    """
    version= 'sypterm'

    pjterm= "./pjterm-macosx-10.8-x86_64"

    def setup(self,create=False):
        """
            setup a sypterm to adapt pjterm


            parameters:
                command_line = "--answer-code 200"


        :return:
        """
        print "Sypterm.__init__(identifier=%s,parameters=%s)" % (str(self.identifier),str(self.parameters))

        create=True

        if create:
            # build an adapter around pjterm
            binary= "%s %s" % (self.pjterm,self.parameters['command_line'])
            print "create pjterm with %s" % binary
            self.adapter= Adapter(self.identifier,binary)
            self.adapter.start()

        return

    def write_stdin(self,message):
        """

        """
        a= ClientAdapter(self.identifier)
        r= a.send(message)
        return r

    def read_stdout(self):
        """

        """
        a= ClientAdapter(self.identifier)
        r= a.read(timeout=1)
        return r


    def dump_status(self):
        """

        """
        self.adapter.send('d\n')

    def close(self):
        """

        """
        a= ClientAdapter(self.identifier)
        a.send('q/n')

        a.exit()
        return



class DroydTerm(CustomAgent,Mobile):
    """
    """
    version= 'droydterm'

    def setup(self,create=False):
        """

        :param identifier:
        :param parameters:
        :return:
        """
        print "Droydterm.__init__(identifier=%s,parameters=%s)" % (str(self.identifier),str(self.parameters))


        self.set_serial(self.identifier)

        #self.mobile= Mobile()
        #self.mobile.set_serial(self.identifier)


    # def press_home(self):
    #     """
    #     """
    #     self.mobile.press_home()


class CustomHub(Hub):
    """


    """
    session_type= 'phone'

    allow_session_reopen = True





if __name__=="__main__":



    sample_conf= [
        [ 'Alice', 'sypterm', {'command_line':""} ],
        [ 'Bob'  , 'droydterm', {} ]
    ]



    def test_complete():
        """

        :return:
        """

        sample_conf= [
            [ 'Alice', 'sypterm', {'command_line':""} ],
            [ 'Bob'  , 'droydterm', {} ]
        ]



        hub= CustomHub()

        s= hub.open_session(sample_conf)


        hub.dummy('Bob', p1='pp')


        time.sleep(2)
        print " read Alice"
        lines= hub.read_stdout('Alice')
        print lines

        print "dump_status on Alice"
        hub.dump_status('Alice')
        time.sleep(2)
        lines= hub.read_stdout('Alice')
        print lines

        print "dump detailed status on Alice"
        hub.write_stdin('Alice', message='dd\n')
        time.sleep(2)
        lines= hub.read_stdout('Alice')
        print lines


        hub.close('Alice')


        #hub.agent('Alice').dummy(p1='p1', p2='p2')
        #hub.agent('Bob').dummy( p1='pp')



        hub.close_session()


    def test_droyterm():
        """

        :return:
        """

        sample_conf= [
            [ '0a9b2e63'  , 'droydterm', {} ]
        ]



        hub= CustomHub()

        s= hub.open_session(sample_conf)


        hub.dummy('0a9b2e63', p1='pp')


        hub.press_home('0a9b2e63')



        hub.close_session()


    test_droyterm()
    #test_complete()



    print



