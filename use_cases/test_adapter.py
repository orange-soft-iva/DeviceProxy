__author__ = 'cocoon'

import time
from dvproxy.adapter import Adapter, ThreadedAdapter, ProcessAdapter

from dvproxy.adapter import ClientAdapter

def test_basic():
    """
        test without redis

    :return:
    """

    p= Adapter("1", "python term.py")

    p.run_process()

    p.send("ping")

    r= p.read()
    print r

    p.send('show')

    r=p.read()
    print r
    r=p.read()
    print r

    r=p.read()

    p.send('exit')


    p.shutdown_process()

    return

def test_with_redis():
    """

        NEED a redis server

    :return:
    """
    p= Adapter("1", "python term.py")

    p.run_process()


    p.reset_queue(p.q_in)
    p.reset_queue(p.q_out)

    p.q_in.put("ping\n")
    p.transfer_out()
    p.transfer_in()
    p.keep_alive()

    print p.get_keep_alive()

    p.transfer_out()
    p.transfer_in()
    p.keep_alive()

    print p.get_keep_alive()

    p.shutdown_process()

    p.get_keep_alive()

    agents= p.agents_status()


    p.keep_alive(clear=True)


    agents = p.agents_status()

    return

def test_redis_commands():

    p= Adapter("agent1", "python term.py")

    agents= p.agents_status()

    print agents
    return


def test_ThreadedAdapter():

    p= ThreadedAdapter('agent2',"python term.py")


    #p.run_process()

    p.start()
    time.sleep(2)

    # print("send ping")
    # p.q_in.put("ping\n")
    # time.sleep(2)
    #
    # response= p.q_out.get(timeout=1)
    # #response= p.q_out.get_nowait()
    # print "response is %s" % response
    #
    # print("send exit")
    # p.q_in.put("exit\n")
    # time.sleep(2)
    #
    # response= p.q_out.get(timeout=1)
    # #response= p.q_out.get_nowait()
    # print "response is %s" % response


    client= ClientAdapter('agent2')

    print "sending ping"
    client.send('ping\n')

    #time.sleep(1)
    response= client.read()
    print "response is %s" % response



    print "send exit"
    client.exit()
    # client.send('exit\n')
    # #time.sleep(1)

    response= client.read()
    print "response is %s" % response

    p.stop()


    return

def test_ProcessAdapter():

    p= ProcessAdapter('agent2',"python term.py")


    #p.run_process()

    p.start()
    time.sleep(2)

    # print("send ping")
    # p.q_in.put("ping\n")
    # time.sleep(2)
    #
    # response= p.q_out.get(timeout=1)
    # #response= p.q_out.get_nowait()
    # print "response is %s" % response
    #
    # print("send exit")
    # p.q_in.put("exit\n")
    # time.sleep(2)
    #
    # response= p.q_out.get(timeout=1)
    # #response= p.q_out.get_nowait()
    # print "response is %s" % response


    client= ClientAdapter('agent2')

    print "sending ping"
    client.send('ping\n')

    #time.sleep(1)
    response= client.read()
    print "response is %s" % response



    print "send exit"
    client.exit()
    # client.send('exit\n')
    # #time.sleep(1)

    response= client.read()
    print "response is %s" % response

    p.stop()


    return




# test_basic()
#test_with_redis()
# test_redis_commands()
#test_ThreadedAdapter()
test_ProcessAdapter()
print

