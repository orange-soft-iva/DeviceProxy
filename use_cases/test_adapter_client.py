__author__ = 'cocoon'


import time
from dvproxy import adapter




import click
from click.testing import CliRunner





def test_agent_adapter():
    """
        test run , send read status kill

    :return:
    """
    runner = CliRunner()

    agent_id= 'agent-1'
    cmd= 'python term.py'

    result = runner.invoke(adapter.agent, [ "run", agent_id, "--cmd",cmd])
    print result.output
    assert result.exit_code == 0

    time.sleep(1)

    result = runner.invoke(adapter.agent, [ "send", agent_id, "ping\n"])
    print result.output
    assert result.exit_code == 0

    time.sleep(2)

    result = runner.invoke(adapter.agent, [ "read", agent_id, "--timeout", "1"])
    print result.output
    #print "read result: %s" % str(result.exit_code)
    assert result.exit_code == 0
    assert "pong" in result.output , "no pong response received"


    result= runner.invoke(adapter.agent,[ "status", agent_id] )
    print result.output
    assert result.exit_code == 0
    assert not "is None" in result.output, "status should NOT be None"

    time.sleep(3)
    result= runner.invoke(adapter.agent,[ "status", agent_id] )
    print result.output
    assert result.exit_code == 0
    assert not "is None" in result.output, "status should NOT be None"


    result= runner.invoke(adapter.agent,[ "kill", agent_id] )
    print result.output
    assert result.exit_code == 0

    time.sleep(2)

    result= runner.invoke(adapter.agent,[ "status", agent_id] )
    print result.output
    assert result.exit_code == 0
    assert "is None" in result.output, "status should be None"

    time.sleep(2)
    result= runner.invoke(adapter.agent,[ "status", agent_id] )
    print result.output
    assert result.exit_code == 0
    assert "is None" in result.output, "status should be None"


    return

def test_agents_adapter():
    """
        test run , send read status kill

    :return:
    """
    runner = CliRunner()

    agents= ['agent-1','agent-2']
    cmd= 'python term.py'

    # list status and kill agents
    result = runner.invoke(adapter.agents, [ "--status", "--kill" ] )
    print result.output
    assert result.exit_code == 0

    time.sleep(1)

    # start agent 1 and agent 2
    for agent_id in agents:
        result = runner.invoke(adapter.agent, [ "run", agent_id, "--cmd",cmd])
        print result.output
        assert result.exit_code == 0

    time.sleep(2)

    result = runner.invoke(adapter.agents, [ "--status" ] )
    print result.output
    assert result.exit_code == 0
    for line in result.output:
        for agent_id in agents:
            if "agent [%s]" % agent_id in line:
                assert not "status is None" in line, "process not running"

    time.sleep(2)

    result = runner.invoke(adapter.agents, [ "--kill" ] )
    print result.output
    assert result.exit_code == 0

    time.sleep(3)

    # check all status is None
    result = runner.invoke(adapter.agents, [ "--status" ] )
    print result.output
    assert result.exit_code == 0
    for line in result.output:
        if "status is" in line:
            assert "status is None" in line, "one process still alive"



    # result = runner.invoke(adapter.agent, [ "send", agent_id, "ping\n"])
    # print result.output
    # assert result.exit_code == 0

    return



if __name__=="__main__":

    test_agent_adapter()
    test_agents_adapter()