"""

    a sample http client usage


"""
import time
from dvproxy.httpclient import HttpClient,json


url= "http://localhost:5000"

Alice= 'Alice'
Bob= '0a9b2e63'

Charlie= '127.0.0.1:5555'

users= ['Alice',Bob]

#         identifier type         parameters

# to enable charlie :  start bluestack android emulator then "adb connect 127.0.0.1:5555"


cnf= [
        [ Alice,   'sypterm',   {'command_line':""}      ],
        [ Bob  ,    'droydterm', {}         ],


        #[ Charlie  ,    'droydterm', {}         ],
    ]


class MyClient(HttpClient):
    """

    """


    def adb_devices(self,**kwargs):
        """
            list of android devices  ( /sessions/-/adb_devices )
        :return:
        """
        data= { 'session_id':'-', 'cmd':'devices'  ,'parameters': kwargs}

        # request to server
        data = json.dumps( data)
        response = self._web_session.post(self.url('/sessions/-/adb_devices' ),data=data)

        response_data = json.loads(response.content)
        #self._session.content = response_data
        # save the session data
        if response.status_code >= 200 and response.status_code < 300:
            return response_data
        else:
            raise RuntimeError('%s' % response.content)


    # def clear_all(self):
    #     """
    #         clear all sessions to release locks (/sessions/-/clear_all)
    #
    #
    #     :return:
    #     """
    #     response = self._web_session.post(self.url('/sessions/-/clear_all' ))
    #
    #     response_data = json.loads(response.content)
    #     if response.status_code >= 200 and response.status_code < 300:
    #         return response_data
    #     else:
    #         raise RuntimeError('%s' % response.content)



def test_basic():


    #client= HttpClient(url=url,agent_factory=agent_factory)
    client= MyClient(url=url)

    #configuration= []
    #for order,user in enumerate(users):
    #    configuration.append([user,cnf[order]])

    client.clear_sessions()



    configuration= cnf

    session=client.open_session(configuration,session_type=None)
    session_id= session['session']

    print client.adb_devices()

    # try:
    #     print client.clear_all()
    # except RuntimeError:
    #     pass
    #
    # client.check_session(session_id=session_id)

    #r= client.session_command('devices')
    #print r

    #
    # test Droyrunner Alice
    #

    res= client.dummy(Bob, p1="v1",p2="v2")

    client.press_home(Bob)



    #client.press_home(Charlie)



    #
    # test syprunner Bob
    #

    res= client.dummy('Alice', p3="v3")

    try:
        # call with a bad function
        res= client.bad_function('Bob', p3="v3")
    except RuntimeError:
        pass

    try:
        # call with a user
        res= client.bad_function('Unknown', p3="v3")
    except RuntimeError:
        pass



    time.sleep(2)
    print " read Alice"
    lines= client.read_stdout('Alice')
    print lines

    print "dump_status on Alice"
    client.dump_status('Alice')
    time.sleep(2)
    lines= client.read_stdout('Alice')
    print lines

    print "dump detailed status on Alice"
    client.write_stdin('Alice', message='dd\n')
    time.sleep(2)
    lines= client.read_stdout('Alice')
    print lines


    #res= client.agent('Alice').dummy( p1="v1",p2="v2")
    #res= client.agent('Bob').dummy( p3="v3")

    client.close_session()

    return



test_basic()
